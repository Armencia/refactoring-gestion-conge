<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypePermissionRepository")
 */
class TypePermission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */

    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Permission", mappedBy="TypePermission")
     */
    private $permissions;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbJour;

    public function __construct()
    {
        $this->permissions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->Nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Permission[]
     */
    public function getPermissions(): Collection
    {
        return $this->permissions;
    }

    public function addPermission(Permission $permission): self
    {
        if (!$this->permissions->contains($permission)) {
            $this->permissions[] = $permission;
            $permission->setTypeDemande($this);
        }

        return $this;
    }

    public function removePermission(Permission $permission): self
    {
        if ($this->permissions->contains($permission)) {
            $this->permissions->removeElement($permission);
            // set the owning side to null (unless already changed)
            if ($permission->getTypeDemande() === $this) {
                $permission->setTypeDemande(null);
            }
        }

        return $this;
    }
    public function __ToString()
    {
      return $this->nom;
    }

    public function getNbJour(): ?int
    {
        return $this->NbJour;
    }

    public function setNbJour(?int $NbJour): self
    {
        $this->NbJour = $NbJour;

        return $this;
    }
}
