<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

class UtilisateurSearch
{
    private $matricule;

    private $nom;

    private $prenom;

    /**
     * @var ArrayCollection
     */
    private $departement;

    /**
     * @return mixed
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * @param mixed $matricule
     * @return UtilisateurSearch
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     * @return UtilisateurSearch
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     * @return UtilisateurSearch
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * @param ArrayCollection $departement
     * @return UtilisateurSearch
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;
        return $this;
    }


}