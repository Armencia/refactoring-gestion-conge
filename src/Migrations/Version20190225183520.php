<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190225183520 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE sauvegarde');
        $this->addSql('DROP TABLE save');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sauvegarde (id INT AUTO_INCREMENT NOT NULL, id_demande_conge_id INT DEFAULT NULL, id_demande_permission_id INT DEFAULT NULL, pdf VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX UNIQ_45699B776F7F864A (id_demande_conge_id), UNIQUE INDEX UNIQ_45699B779FE9CA7A (id_demande_permission_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE save (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, pdf VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE sauvegarde ADD CONSTRAINT FK_45699B776F7F864A FOREIGN KEY (id_demande_conge_id) REFERENCES conge (id)');
        $this->addSql('ALTER TABLE sauvegarde ADD CONSTRAINT FK_45699B779FE9CA7A FOREIGN KEY (id_demande_permission_id) REFERENCES permission (id)');
    }
}
