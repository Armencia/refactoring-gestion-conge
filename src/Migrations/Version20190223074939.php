<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190223074939 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE demande DROP FOREIGN KEY FK_2694D7A58CF5D39B');
        $this->addSql('DROP TABLE demande');
        $this->addSql('DROP TABLE demande_conge');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE demande (id INT AUTO_INCREMENT NOT NULL, conge_id INT DEFAULT NULL, demande_conge_id INT DEFAULT NULL, qte INT NOT NULL, INDEX IDX_2694D7A58CF5D39B (demande_conge_id), INDEX IDX_2694D7A5CAAC9A59 (conge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE demande_conge (id INT AUTO_INCREMENT NOT NULL, status TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE demande ADD CONSTRAINT FK_2694D7A58CF5D39B FOREIGN KEY (demande_conge_id) REFERENCES demande_conge (id)');
        $this->addSql('ALTER TABLE demande ADD CONSTRAINT FK_2694D7A5CAAC9A59 FOREIGN KEY (conge_id) REFERENCES conge (id)');
    }
}
