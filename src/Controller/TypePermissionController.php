<?php

namespace App\Controller;

use App\Entity\TypePermission;
use App\Form\TypePermissionType;
use App\Repository\TypePermissionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type/permission")
 */
class TypePermissionController extends AbstractController
{
    /**
     * @Route("/", name="type_permission_index", methods={"GET"})
     */
    public function index(TypePermissionRepository $typePermissionRepository): Response
    {
        return $this->render('type_permission/index.html.twig', [
            'type_permissions' => $typePermissionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="type_permission_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typePermission = new TypePermission();
        $form = $this->createForm(TypePermissionType::class, $typePermission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typePermission);
            $entityManager->flush();

            return $this->redirectToRoute('type_permission_index');
        }

        return $this->render('type_permission/new.html.twig', [
            'type_permission' => $typePermission,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_permission_show", methods={"GET"})
     */
    public function show(TypePermission $typePermission): Response
    {
        return $this->render('type_permission/show.html.twig', [
            'type_permission' => $typePermission,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_permission_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypePermission $typePermission): Response
    {
        $form = $this->createForm(TypePermissionType::class, $typePermission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_permission_index', [
                'id' => $typePermission->getId(),
            ]);
        }

        return $this->render('type_permission/edit.html.twig', [
            'type_permission' => $typePermission,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_permission_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypePermission $typePermission): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typePermission->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typePermission);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_permission_index');
    }
}
