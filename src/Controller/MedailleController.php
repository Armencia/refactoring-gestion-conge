<?php

namespace App\Controller;

use App\Entity\Medaille;
use App\Form\MedailleType;
use App\Repository\MedailleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/medaille")
 */
class MedailleController extends AbstractController
{
    /**
     * @Route("/", name="medaille_index", methods="GET")
     */
    public function index(MedailleRepository $medailleRepository): Response
    {
        return $this->render('medaille/index.html.twig', ['medailles' => $medailleRepository->findAll()]);
    }

    /**
     * @Route("/new", name="medaille_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $medaille = new Medaille();
        $form = $this->createForm(MedailleType::class, $medaille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($medaille);
            $em->flush();

            return $this->redirectToRoute('medaille_index');
        }

        return $this->render('medaille/new.html.twig', [
            'medaille' => $medaille,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="medaille_show", methods="GET")
     */
    public function show(Medaille $medaille): Response
    {
        return $this->render('medaille/show.html.twig', ['medaille' => $medaille]);
    }

    /**
     * @Route("/{id}/edit", name="medaille_edit", methods="GET|POST")
     */
    public function edit(Request $request, Medaille $medaille): Response
    {
        $form = $this->createForm(MedailleType::class, $medaille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('medaille_index', ['id' => $medaille->getId()]);
        }

        return $this->render('medaille/edit.html.twig', [
            'medaille' => $medaille,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="medaille_delete", methods="DELETE")
     */
    public function delete(Request $request, Medaille $medaille): Response
    {
        if ($this->isCsrfTokenValid('delete'.$medaille->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($medaille);
            $em->flush();
        }

        return $this->redirectToRoute('medaille_index');
    }
}
