<?php

namespace App\Controller;

use App\Entity\Conge;
use App\Form\CongeType;
use App\Repository\CongeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
/**
 * @Route("/conge")
 */
class CongeController extends AbstractController
{
  /**
  *@Route("/new/dompdf/{id}" , name="conge_pdf")
  **/
  public function toPdfAction(Request $request ,Conge $conge): Response
  {
    $this->show($conge);

    // Configure Dompdf according to your needs
     $pdfOptions = new Options();
     $pdfOptions->set('defaultFont', 'Arial');


     // Instantiate Dompdf with our options
     $dompdf = new Dompdf($pdfOptions);

     // Retrieve the HTML generated in our twig file

     $html = $this->renderView('conge/pdf.html.twig', [
         'title' => "Welcome to our PDF Test",
          'conge' => $conge,

     ]);

     // Load HTML to Dompdf
     $dompdf->loadHtml($html);

     // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
     $dompdf->setPaper('A4', 'portrait');

     // Render the HTML as PDF
     $dompdf->render();

     // Output the generated PDF to Browser (inline view)
     $dompdf->stream("pdfConge.pdf", [
         "Attachment" => false
     ]);
  }

    /**
     * @Route("/pdf/{id}", name="pdf_test", methods={"GET"})
     */
    public function test(Conge $conge): Response
    {
        return $this->render('conge/pdf.html.twig', [
            'conge' => $conge,
        ]);
    }

    /**
     * @Route("/", name="conge_index", methods={"GET"})
     * Lister les congés enregistrés dont le status est False
     * Prend en paramètre l'instance de la classe permettant de faire des requêtes sur l'entité Congé
     */
    public function index(CongeRepository $congeRepository): Response
    {
        //renvoie la vue listant les congés dont le status est False
        return $this->render('conge/index.html.twig', [
            'conges' => $congeRepository->findStatusFalse(),
        ]);
    }

    /**
     * @Route("/new", name="conge_new", methods={"GET","POST"})
     * Ajouter un nouveau congé
     */
    public function newConge(Request $request): Response
    {
        //création d'une instance de la classe Congé
        $conge = new Conge();
        
        //création d'un formulaire à l'image de l'instance précédemment créé
        $form = $this->createForm(CongeType::class, $conge); 
        
        //écoute de la requête
        $form->handleRequest($request);

        //test si le formulaire est valide et envoyé
        if ($form->isSubmitted() && $form->isValid()) {
            
            //Donner la date du jour à l'attribut DateDemande de la classe Congé
            $conge->setDateDemande(new \DateTime());
            
            //Mettre le statut du congé à False pour dire que le congé n'est pas encore envoyé à l'administrateur
            $conge->setStatus(false);
            //récupération de l'intance de l'ORM pour l'enregistrement
            $entityManager = $this->getDoctrine()->getManager();
            
            //création de la requête pour l'enregistrement
            $entityManager->persist($conge);
            
            //execution de la requête
            $entityManager->flush();
            
            //redirection vers la liste des congés
            return $this->redirectToRoute('conge_index');
        }
        
        //renvoie le formulaire pour la création du congé
        return $this->render('conge/new.html.twig', [
            'conge' => $conge,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="conge_show", methods={"GET"})
     */
    public function show(Conge $conge): Response
    {
        return $this->render('conge/show.html.twig', [
            'conge' => $conge,
        ]);
    }

    /**
     * @Route("/envoyer/{id}", name="congeEnvoyer_show", methods={"GET"})
     */
    public function showEnvoyer(Conge $conge): Response
    {
        return $this->render('conge/showCongeEnvoyer.html.twig', [
            'conge' => $conge,
        ]);
    }


    /**
     * @Route("/recue/{id}", name="congeRecue_show", methods={"GET"})
     */
    public function showRecue(Conge $conge): Response
    {
        return $this->render('conge/showCongeRecue.html.twig', [
            'conge' => $conge,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="conge_edit", methods={"GET","POST"})
     * Modification d'un congé
     * Prend en paramètre le congé à modifier
     */
    public function edit(Request $request, Conge $conge): Response
    {
        //création du formulaire à l'image de la classe congé reçu en paramètre
        $form = $this->createForm(CongeType::class, $conge);
        
        //écoute de la requête et modification
        $form->handleRequest($request);

        //test si le formulaire est valide et a été envoyé
        if ($form->isSubmitted() && $form->isValid()) {
            //execution de la requête de modification via Doctrine
            $this->getDoctrine()->getManager()->flush();
            
            //redirection vers le liste des congés
            return $this->redirectToRoute('conge_index', [
                'id' => $conge->getId(),
            ]);
        }
        
        //renvoie du formulaire
        return $this->render('conge/edit.html.twig', [
            'conge' => $conge,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="conge_delete", methods={"DELETE"})
     * Suppression d'un congé
     * Prend en paramètre le congé à supprimer
     */
    public function delete(Request $request, Conge $conge): Response
    {
        //Vérification du token envoyé pour la sécurité
        if ($this->isCsrfTokenValid('delete'.$conge->getId(), $request->request->get('_token'))) {
            //récupération de l'intance de l'ORM pour l'enregistrement
            $entityManager = $this->getDoctrine()->getManager();
            
            //préparation de la requête de suppression
            $entityManager->remove($conge);
            
            //execution de la requête 
            $entityManager->flush();
        }
        
        //redirection vers la liste des congés
        return $this->redirectToRoute('conge_index');
    }

    /**
     * @Route("/conger/envoyer", name="indexCogeEnvoyer")
     */
    public function indexCongeEnvoyer( CongeRepository $congeRepository)
    {
        $conge = $congeRepository->findStatusTrue();
        return $this->render('conge/CongeEnvoyer.html.twig',[
            'conges' => $conge
            ]);
    }

    /**
     * @Route("/conger/recue", name="indexCongeRecu")
     */
    public function indexCongeRecu( CongeRepository $congeRepository)
    {
        $conge = $congeRepository->findStatusTrue();
        return $this->render('conge/congerRecue.html.twig',[
            'conges' => $conge
        ]);
    }

    /**
     * @Route("/envoyera/{id}", name="envoyerCongeaa")
     */
    public function envoyer(Conge $conge)
    {
        $em = $this->getDoctrine()->getManager();

        $conge->setStatus(true);
        $nombre = $conge->getNombre();
        $personne= $conge->getUtilisateur();

        $total = $personne->getNbJour();
        $total = $total - $nombre;

        $personne->setNbJour($total);

        $em->persist($personne);
//        dump($total);
//        die();


     $em->flush();
     return $this->redirectToRoute('indexCogeEnvoyer');

    }


    /**
     * @Route("/ swift/conge/accepter", name="mailer")
     *
     */
    public function indexSwift(\Swift_Mailer $mailer)
    {

        $message = (new \Swift_Message('Demande Accepter'))
            ->setFrom('send@example.com')
            ->setTo('taniaramanandraibe23@gmail.com')
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'security/accepter.html.twig'),
                'text/html'
            )
        ;

        $mailer->send($message);

        return $this->redirectToRoute('indexCogeEnvoyer');
    }

    /**
     * @Route("/swift/conge/refuser", name="mailerRefuser")
     *
     */
    public function indexSwiftRefuser(\Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Demande Refuser'))
            ->setFrom('send@example.com')
            ->setTo('taniaramanandraibe23@gmail.com')
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'security/refuser.html.twig'),
                'text/html'
            )
        ;

        $mailer->send($message);

        return $this->redirectToRoute('indexCogeEnvoyer');
    }
}
