<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
    *@Route("login", name="login")
    */
    public function login()
    {
      return $this->render('security/newlogin.html.twig');
    }

    /**
     * @Route("/logouta", name="logouta")
     */
    public function logout()
    {
        $session = $this->get('session');
        $session->clear();

        return $this->redirectToRoute('home');
    }


    /**
     * @Route("/verifyLogin", name="verify_login")
     */
    public function verifyLogin(UtilisateurRepository $utilisateurRepository)
    {
        if ($user = $utilisateurRepository->findAuth($_POST['_username'], $_POST['_password']))
        {
        $user = $utilisateurRepository->findAuth($_POST['_username'], $_POST['_password']);
        $session = $this->get('session');
        $session->set('user', $user);
        $myUser = $session->get('user');
//        dump($myUser);
//        die();

        return $this->redirectToRoute('apropos');
        }
        else{
            return $this->redirectToRoute('login');
        }
    }

    public function menu()
    {
        $session = $this->get('session');
        $theUser = $session->get('user');
        $role = $theUser[0]->getRole();
        dump($theUser);

        return $this->render('home/menu.html.twig',[
            'role'=>$role
        ]);
    }

    /**
     * @Route("/profil", name="profil")
     */
    public function profil(UtilisateurRepository $userRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        $myUser = $session->get('user');
        $user = $session->get('user');
        $session->set('user',$user);
//
//        dump($user);
//        die();
        $user = $em->getRepository(Utilisateur::class)->find($user[0]->getId());



        return $this->render('home/profil.html.twig', [
            'user' => $user
        ]);

    }



    /**
     * @Route("/apropos", name="apropos")
     */
        public function propos(UtilisateurRepository $userRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        $myUser = $session->get('user');
        $user = $session->get('user');
        $session->set('user',$user);
//
//        dump($user);
//        die();
        $user = $em->getRepository(Utilisateur::class)->find($user[0]->getId());



        return $this->render('home/apropos.html.twig', [
            'user' => $user
        ]);

    }

    /**
     * @Route("/test", name="test")
     */
    public function test()
    {
        return $this->render('security/refuser.html.twig');
    }

    /**
     * @Route("/bookings/calendar", name="booking")
     */
    public function booking()
    {
        return $this->render('security/booking.html.twig');
    }

    /**
     * @Route("/accueil", name="accueil")
     */
    public function accueil()
    {
     return $this->render('home/accueil.html.twig');
    }


}
