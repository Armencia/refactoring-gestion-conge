<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GererController extends AbstractController
{
    /**
     * @Route("/gerer", name="gerer")
     */
    public function index()
    {
        return $this->render('gerer/index.html.twig', [
            'controller_name' => 'GererController',
        ]);
    }
}
