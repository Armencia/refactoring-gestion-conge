<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Entity\Role;
use App\Entity\Departement;
use App\Entity\Medaille;

use App\Entity\UtilisateurSearch;
use App\Form\UtilisateurSearchType;
use App\Form\UtilisateurType;
use App\Repository\UtilisateurRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/utilisateur")
 */
class UtilisateurController extends AbstractController
{
    /**
     * @Route("/", name="utilisateur_index", methods="GET")
     */
    public function index(PaginatorInterface $paginator, Request $request, UtilisateurRepository $utilisateurRepository): Response
    {
        $search = new UtilisateurSearch();
        $form = $this->createForm(UtilisateurSearchType::class, $search);
//        $form->handleRequest();

        $utilisateur = $paginator->paginate(
            $utilisateurRepository->findAllVisibleQuery($search),
            $request->query->getInt('page',1),
            20
    );
        return $this->render('utilisateur/index.html.twig', [
            'utilisateurs' => $utilisateur,
            'formSearch' => $form->createView()
        ]);
    }

    /**
     * @Route("/accueil", name="utilisateur_accueil", methods="GET")
     */
    public function accueil(UtilisateurRepository $utilisateurRepository): Response
    {
        return $this->render('utilisateur/accueil.html.twig', ['utilisateurs' => $utilisateurRepository->findAll()]);
    }

    /**
     * @Route("/new", name="utilisateur_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $utilisateur = new Utilisateur();
        $form = $this->createForm(UtilisateurType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($utilisateur);
            if($utilisateur->getMedaille()== 'Or'){
                $utilisateur->setNbJour(35);
            }
            elseif($utilisateur->getMedaille()=='Vermeil')
            {
                $utilisateur->setNbJour(32);
            }
            elseif($utilisateur->getMedaille()=='Argent')
            {
                $utilisateur->setNbJour(31);
            }
            else{
            $utilisateur->setNbJour(30);
            }
            $em->flush();

            return $this->redirectToRoute('utilisateur_index');
        }

        return $this->render('utilisateur/new.html.twig', [
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="utilisateur_show", methods="GET")
     */
    public function show(Utilisateur $utilisateur): Response
    {
        return $this->render('utilisateur/show.html.twig', ['utilisateur' => $utilisateur]);
    }

    /**
     * @Route("/{id}/edit", name="utilisateur_edit", methods="GET|POST")
     */
    public function edit(Request $request, Utilisateur $utilisateur): Response
    {
        $form = $this->createForm(UtilisateurType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('utilisateur_index', ['id' => $utilisateur->getId()]);
        }

        return $this->render('utilisateur/edit.html.twig', [
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="utilisateur_delete", methods="DELETE")
     */
    public function delete(Request $request, Utilisateur $utilisateur): Response
    {
        if ($this->isCsrfTokenValid('delete'.$utilisateur->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($utilisateur);
            $em->flush();
        }

        return $this->redirectToRoute('utilisateur_index');
    }

  


}
