<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SnappyController extends AbstractController
{
  private $knpSnappy;

  public function __construct(\Knp\Snappy\Pdf $knpSnappy)
  {
  $this->knpSnappy = $knpSnappy;
  }
    /**
     * @Route("/snappy", name="snappy")
     */

    public function indexAction(Request $request)
    {

      $html = $this->render("conge/pdf.html.twig", array(
        "title" => "pdf"
      ));
      $filename ="custom_pdf_from_twig";
      return new Response(
        $this->knpSnappy->getOutputFromHtml($html),
        200,
        array(
          'Content-Type' => 'application/pdf',
          'Content-Disposition' => 'inline; filename="'.$filename.'.pdf"'
        )
      );
    }
}
