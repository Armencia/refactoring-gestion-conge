<?php

namespace App\Controller;

use App\Entity\Permission;
use App\Entity\TypePermission;
use App\Form\PermissionType;
use App\Repository\PermissionRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/permission")
 */
class PermissionController extends AbstractController
{
    /**
     * @Route("/", name="permission_index", methods={"GET"})
     */
    public function index(PermissionRepository $permissionRepository): Response
    {
        return $this->render('permission/index.html.twig', [
            'permissions' => $permissionRepository->findStatusFalse(),
        ]);
    }

    /**
     * @Route("/new", name="permission_new", methods={"GET","POST"})
     */
    public function newPermission(Request $request): Response
    {
        $permission = new Permission();
        $form = $this->createForm(PermissionType::class, $permission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($permission);
            $permission->setDateDemande(new \DateTime('now'));
            $permission->setStatus(false);
            $entityManager->flush();

            return $this->redirectToRoute('permission_index');
        }

        return $this->render('permission/new.html.twig', [
            'permission' => $permission,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/permission/envoyer", name="indexPermissionEnvoyer")
     */
    public function indexCongeEnvoyer( PermissionRepository $permissionRepository)
    {
        $permission = $permissionRepository->findStatusTrue();
        return $this->render('permission/PermissionEnvoyer.html.twig',[
            'permissions' => $permission
        ]);
    }


    /**
     * @Route("/permission/recue", name="indexPermissionRecu")
     */
    public function indexCongeRecu( PermissionRepository $permissionRepository)
    {
        $permission = $permissionRepository->findStatusTrue();
        return $this->render('permission/Permissionrecue.html.twig',[
            'permissions' => $permission
        ]);
    }

    /**
     * @Route("/envoyer/parmission/{id}", name="envoyerPermission")
     */
    public function envoyer(Permission $permission)
    {
        $permission->setStatus(true);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return $this->redirectToRoute('indexPermissionEnvoyer');

    }

    /**
     * @Route("/{id}", name="permission_show", methods={"GET"})
     */
    public function show(Permission $permission): Response
    {
        return $this->render('permission/show.html.twig', [
            'permission' => $permission,
        ]);
    }

    /**
     * @Route("/pemission/envoyer/{id}", name="permissionEnvoyer_show", methods={"GET"})
     */
    public function showEnvoyer(Permission $permission): Response
    {
        return $this->render('permission/showPermissionEnvoyer.html.twig', [
            'permission' => $permission,
        ]);
    }

    /**
     * @Route("/pemission/recue/{id}", name="permissionRecue_show", methods={"GET"})
     */
    public function showRecue(Permission $permission): Response
    {
        return $this->render('permission/showPermissionRecue.html.twig', [
            'permission' => $permission,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="permission_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Permission $permission): Response
    {
        $form = $this->createForm(PermissionType::class, $permission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('permission_index', [
                'id' => $permission->getId(),
            ]);
        }

        return $this->render('permission/edit.html.twig', [
            'permission' => $permission,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="permission_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Permission $permission): Response
    {
        if ($this->isCsrfTokenValid('delete'.$permission->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($permission);
            $entityManager->flush();
        }

        return $this->redirectToRoute('permission_index');
    }


    /**
     * @Route("/ swift/permission/accepter", name="mailerPermission")
     *
     */
    public function indexSwift(\Swift_Mailer $mailer)
    {

        $message = (new \Swift_Message('Demande Accepter'))
            ->setFrom('send@example.com')
            ->setTo('taniaramanandraibe23@gmail.com')
            ->setBody(
                $this->renderView(
                    'security/accepterPermission.html.twig'),
                'text/html'
            )
        ;

        $mailer->send($message);

        return $this->redirectToRoute('indexPermissionRecu');
    }

    /**
     * @Route("/swift/refuser", name="mailerRefuserPermission")
     *
     */
    public function indexSwiftRefuser(\Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Demande Refuser'))
            ->setFrom('send@example.com')
            ->setTo('taniaramanandraibe23@gmail.com')
            ->setBody(
                $this->renderView(
                    'security/refuserPermission.html.twig'),
                'text/html'
            )
        ;

        $mailer->send($message);

        return $this->redirectToRoute('indexPermissionRecu');
    }


    /**
     *@Route("/new/Permission/dompdf/{id}" , name="Permission_pdf")
     **/
    public function toPdfAction(Request $request ,Permission $permission): Response
    {
        $this->show($permission);

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');


        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file

        $html = $this->renderView('permission/pdf.html.twig', [
            'title' => "Welcome to our PDF Test",
            'permission' => $permission,

        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (inline view)
        $dompdf->stream("pdfPermission.pdf", [
            "Attachment" => false
        ]);
    }
}
