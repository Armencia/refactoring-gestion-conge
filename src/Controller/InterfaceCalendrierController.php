<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InterfaceCalendrierController extends AbstractController
{
    /**
     * @Route("/interface/calendrier", name="interface_calendrier")
     */
    public function index()
    {
        return $this->render('interface_calendrier/index.html.twig', [
            'controller_name' => 'InterfaceCalendrierController',
        ]);
    }
}
