<?php

namespace App\Controller;

use App\Entity\Conge;
use App\Repository\CongeRepository;
use App\Form\CongeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Dompdf\Dompdf;
use Dompdf\Options;

class DompdfController extends AbstractController
{
    /**
     * @Route("/dompdf", name="dompdf")
     */
    public function toPdfAction(Request $request)
    {
      $conge = new Conge();
      $form = $this->createForm(CongeType::class, $conge);
      $form->handleRequest($request);

      // Configure Dompdf according to your needs
       $pdfOptions = new Options();
       $pdfOptions->set('defaultFont', 'Arial');


       // Instantiate Dompdf with our options
       $dompdf = new Dompdf($pdfOptions);

       // Retrieve the HTML generated in our twig file

       $html = $this->renderView('conge/pdf.html.twig', [
           'title' => "Welcome to our PDF Test"
       ]);

       // Load HTML to Dompdf
       $dompdf->loadHtml($html);

       // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
       $dompdf->setPaper('A4', 'portrait');

       // Render the HTML as PDF
       $dompdf->render();

       // Output the generated PDF to Browser (inline view)
       $dompdf->stream("mypdf.pdf", [
           "Attachment" => false
       ]);
   }
}
