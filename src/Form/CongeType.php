<?php

namespace App\Form;

use App\Entity\Conge;
use App\Entity\TypeConge;
use App\Entity\Utilisateur;
use App\Entity\Departement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class CongeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('utilisateur' , EntityType::class,[
              'label' => 'N° Matricule',
              'class' => Utilisateur::class,
              'choice_label' => 'matricule',
            ])
            ->add('CIN', TextType::class, [
                'label' => 'N° CIN',
                'attr' => [
                    'placeholder' => 'Enter votre numero Numéro CIN'
                ]
            ])
            ->add('email', TextType::class, [
                'label' => 'Email de Confirmation',
                'attr' => [
                    'placeholder' => 'Confirmez votre adresse email'
                ]
            ])
            ->add('TypeConge', EntityType::class,[
              'class' => TypeConge::class,
              'choice_label' => 'nom',
            ])

            ->add('raison', TextType::class, [
                'label' => 'Raison',
                'required'=>false,
                'attr' => [
                    'placeholder' => 'Si vous avez choisi autre type de congé, ce champ est obligatoire'
                ]
            ])

            ->add('nombre', TextType::class, [
                'label' => 'Nombres de jours',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Si vous avez choisi autre type de congé, ce champ est obligatoire'
                ]
            ])

            ->add('dateDebut' , DateTimeType::class, [
              'widget' => 'single_text',
                    'html5' => false,
                    'required' => true,
                    'attr' => array('class' => 'form-control input-inline datetimepicker',
                                     'data-provide' => 'datetimepicker',
                                     'data-format' => 'dd-mm-yyyy HH:ii',
                                        ),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Conge::class,
        ]);
    }
}
