<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Role;
use App\Entity\Departement;
use App\Entity\Medaille;

class UtilisateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('matricule')
            ->add('nom')
            ->add('prenom')
            ->add('CIN', TextType::class, [
                'label' => 'Numéro CIN'
            ])
            ->add('email')
            ->add('imageFile', FileType::class,[
                'required' => false
            ])
            ->add('Username')
            ->add('password')
                        ->add('departement', EntityType::class,[
              'class'=> Departement::class,
              'choice_label' => 'nom'
            ])
            ->add('medaille', EntityType::class, [
              'class'=> Medaille::class,
              'choice_label' => 'nom'
            ])
            ->add('role', CheckboxType::class, [
                'label' => 'Chef hierarchique',
                'required' => false
            ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
