<?php

namespace App\Form;

use App\Entity\UtilisateurSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilisateurSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('matricule', TextType::class, [
                'required' =>false,
                'label'    =>false,
                'attr'     => [
                    'placeholder' => 'N° Matricule'
                ]
            ])
            ->add('nom', TextType::class, [
                'required' =>false,
                'label'    =>false,
                'attr'     => [
                    'placeholder' => 'Nom'
                ]
            ])
            ->add('prenom', TextType::class, [
                'required' =>false,
                'label'    =>false,
                'attr'     => [
                    'placeholder' => 'Prénoms'
                ]
            ])
            ->add('departement', TextType::class, [
                'required' =>false,
                'label'    =>false,
                'attr'     => [
                    'placeholder' => 'Département'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UtilisateurSearch::class,
            'method'    => 'get',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
