<?php

namespace App\Repository;

use App\Entity\Utilisateur;
use App\Entity\UtilisateurSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Utilisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utilisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utilisateur[]    findAll()
 * @method Utilisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Utilisateur::class);
    }


    // /**
    //  * @return Utilisateur[] Returns an array of Utilisateur objects
    //  */

    public function findAuth($login, $password)
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.Username = :login')
            ->andWhere('u.Password = :password')
            ->setParameter('login', $login)
            ->setParameter('password', $password)
            ->getQuery()
            ->getResult()
        ;
    }


    public function findAllVisibleQuery(UtilisateurSearch $utilisateurSearch)
    {
        $query = $this->findAllVisible();

        if($utilisateurSearch->getMatricule()){
            $query = $query

                ->andWhere('m.matricule = matricule')
                ->setParameter('matricule', '%'.$utilisateurSearch->getMatricule().'%');
        }

        if ($utilisateurSearch->getNom()){
            $query =$query
                ->andWhere('m.nom = nom')
                ->setParameter('nom', '%'.$utilisateurSearch->getNom().'%');
        }

        if ($utilisateurSearch->getPrenom()){
            $query = $query
                ->andWhere('m.prenom = prenom')
                ->setParameter('prenom', '%'.$utilisateurSearch->getPrenom().'%');
        }

        if ($utilisateurSearch->getDepartement()){
            foreach ($utilisateurSearch->getDepartement() as $k => $departement){

                $query = $query
                    ->andWhere(":departement$k MEMBER OF m.departement")
                    ->setParameter("departement$k", $departement);
            }
        }

        return $query->getQuery();
    }

    public function findAllVisible()
    {
        return $this->createQueryBuilder('m')
            ->orderBy('m.id', 'ASC');
//            ->getQuery()
//            ->getResult();
    }
    /*
    public function findOneBySomeField($value): ?Utilisateur
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
