<?php

namespace App\Repository;

use App\Entity\NbJours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NbJours|null find($id, $lockMode = null, $lockVersion = null)
 * @method NbJours|null findOneBy(array $criteria, array $orderBy = null)
 * @method NbJours[]    findAll()
 * @method NbJours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NbJoursRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NbJours::class);
    }

    // /**
    //  * @return NbJours[] Returns an array of NbJours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NbJours
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
