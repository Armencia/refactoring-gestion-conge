<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = [
        'booking_index' => [[], ['_controller' => 'App\\Controller\\BookingController::index'], [], [['text', '/booking/']], [], []],
        'booking_new' => [[], ['_controller' => 'App\\Controller\\BookingController::new'], [], [['text', '/booking/new']], [], []],
        'booking_show' => [['id'], ['_controller' => 'App\\Controller\\BookingController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/booking']], [], []],
        'booking_edit' => [['id'], ['_controller' => 'App\\Controller\\BookingController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/booking']], [], []],
        'booking_delete' => [['id'], ['_controller' => 'App\\Controller\\BookingController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/booking']], [], []],
        'conge_pdf' => [['id'], ['_controller' => 'App\\Controller\\CongeController::toPdfAction'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/conge/new/dompdf']], [], []],
        'pdf_test' => [['id'], ['_controller' => 'App\\Controller\\CongeController::test'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/conge/pdf']], [], []],
        'conge_index' => [[], ['_controller' => 'App\\Controller\\CongeController::index'], [], [['text', '/conge/']], [], []],
        'conge_new' => [[], ['_controller' => 'App\\Controller\\CongeController::newConge'], [], [['text', '/conge/new-test']], [], []],
        'conge_show' => [['id'], ['_controller' => 'App\\Controller\\CongeController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/conge']], [], []],
        'congeEnvoyer_show' => [['id'], ['_controller' => 'App\\Controller\\CongeController::showEnvoyer'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/conge/envoyer']], [], []],
        'congeRecue_show' => [['id'], ['_controller' => 'App\\Controller\\CongeController::showRecue'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/conge/recue']], [], []],
        'conge_edit' => [['id'], ['_controller' => 'App\\Controller\\CongeController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/conge']], [], []],
        'conge_delete' => [['id'], ['_controller' => 'App\\Controller\\CongeController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/conge']], [], []],
        'indexCogeEnvoyer' => [[], ['_controller' => 'App\\Controller\\CongeController::indexCongeEnvoyer'], [], [['text', '/conge/conger/envoyer']], [], []],
        'indexCongeRecu' => [[], ['_controller' => 'App\\Controller\\CongeController::indexCongeRecu'], [], [['text', '/conge/conger/recue']], [], []],
        'envoyerCongeaa' => [['id'], ['_controller' => 'App\\Controller\\CongeController::envoyer'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/conge/envoyera']], [], []],
        'mailer' => [[], ['_controller' => 'App\\Controller\\CongeController::indexSwift'], [], [['text', '/conge/ swift/conge/accepter']], [], []],
        'mailerRefuser' => [[], ['_controller' => 'App\\Controller\\CongeController::indexSwiftRefuser'], [], [['text', '/conge/swift/conge/refuser']], [], []],
        'departement_index' => [[], ['_controller' => 'App\\Controller\\DepartementController::index'], [], [['text', '/departement/']], [], []],
        'departement_new' => [[], ['_controller' => 'App\\Controller\\DepartementController::new'], [], [['text', '/departement/new']], [], []],
        'departement_show' => [['id'], ['_controller' => 'App\\Controller\\DepartementController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/departement']], [], []],
        'departement_edit' => [['id'], ['_controller' => 'App\\Controller\\DepartementController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/departement']], [], []],
        'departement_delete' => [['id'], ['_controller' => 'App\\Controller\\DepartementController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/departement']], [], []],
        'dompdf' => [[], ['_controller' => 'App\\Controller\\DompdfController::toPdfAction'], [], [['text', '/dompdf']], [], []],
        'gerer' => [[], ['_controller' => 'App\\Controller\\GererController::index'], [], [['text', '/gerer']], [], []],
        'gestion' => [[], ['_controller' => 'App\\Controller\\GestionController::index'], [], [['text', '/gestion']], [], []],
        'home' => [[], ['_controller' => 'App\\Controller\\HomeController::index'], [], [['text', '/']], [], []],
        'interface_calendrier' => [[], ['_controller' => 'App\\Controller\\InterfaceCalendrierController::index'], [], [['text', '/interface/calendrier']], [], []],
        'medaille_index' => [[], ['_controller' => 'App\\Controller\\MedailleController::index'], [], [['text', '/medaille/']], [], []],
        'medaille_new' => [[], ['_controller' => 'App\\Controller\\MedailleController::new'], [], [['text', '/medaille/new']], [], []],
        'medaille_show' => [['id'], ['_controller' => 'App\\Controller\\MedailleController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/medaille']], [], []],
        'medaille_edit' => [['id'], ['_controller' => 'App\\Controller\\MedailleController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/medaille']], [], []],
        'medaille_delete' => [['id'], ['_controller' => 'App\\Controller\\MedailleController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/medaille']], [], []],
        'permission_index' => [[], ['_controller' => 'App\\Controller\\PermissionController::index'], [], [['text', '/permission/']], [], []],
        'permission_new' => [[], ['_controller' => 'App\\Controller\\PermissionController::newPermission'], [], [['text', '/permission/new']], [], []],
        'indexPermissionEnvoyer' => [[], ['_controller' => 'App\\Controller\\PermissionController::indexCongeEnvoyer'], [], [['text', '/permission/permission/envoyer']], [], []],
        'indexPermissionRecu' => [[], ['_controller' => 'App\\Controller\\PermissionController::indexCongeRecu'], [], [['text', '/permission/permission/recue']], [], []],
        'envoyerPermission' => [['id'], ['_controller' => 'App\\Controller\\PermissionController::envoyer'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/permission/envoyer/parmission']], [], []],
        'permission_show' => [['id'], ['_controller' => 'App\\Controller\\PermissionController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/permission']], [], []],
        'permissionEnvoyer_show' => [['id'], ['_controller' => 'App\\Controller\\PermissionController::showEnvoyer'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/permission/pemission/envoyer']], [], []],
        'permissionRecue_show' => [['id'], ['_controller' => 'App\\Controller\\PermissionController::showRecue'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/permission/pemission/recue']], [], []],
        'permission_edit' => [['id'], ['_controller' => 'App\\Controller\\PermissionController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/permission']], [], []],
        'permission_delete' => [['id'], ['_controller' => 'App\\Controller\\PermissionController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/permission']], [], []],
        'mailerPermission' => [[], ['_controller' => 'App\\Controller\\PermissionController::indexSwift'], [], [['text', '/permission/ swift/permission/accepter']], [], []],
        'mailerRefuserPermission' => [[], ['_controller' => 'App\\Controller\\PermissionController::indexSwiftRefuser'], [], [['text', '/permission/swift/refuser']], [], []],
        'Permission_pdf' => [['id'], ['_controller' => 'App\\Controller\\PermissionController::toPdfAction'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/permission/new/Permission/dompdf']], [], []],
        'planning' => [[], ['_controller' => 'App\\Controller\\PlanningController::index'], [], [['text', '/planning']], [], []],
        'recherche' => [[], ['_controller' => 'App\\Controller\\RechercheController::index'], [], [['text', '/recherche']], [], []],
        'login' => [[], ['_controller' => 'App\\Controller\\SecurityController::login'], [], [['text', '/login']], [], []],
        'logouta' => [[], ['_controller' => 'App\\Controller\\SecurityController::logout'], [], [['text', '/logouta']], [], []],
        'verify_login' => [[], ['_controller' => 'App\\Controller\\SecurityController::verifyLogin'], [], [['text', '/verifyLogin']], [], []],
        'profil' => [[], ['_controller' => 'App\\Controller\\SecurityController::profil'], [], [['text', '/profil']], [], []],
        'apropos' => [[], ['_controller' => 'App\\Controller\\SecurityController::propos'], [], [['text', '/apropos']], [], []],
        'test' => [[], ['_controller' => 'App\\Controller\\SecurityController::test'], [], [['text', '/test']], [], []],
        'booking' => [[], ['_controller' => 'App\\Controller\\SecurityController::booking'], [], [['text', '/bookings/calendar']], [], []],
        'accueil' => [[], ['_controller' => 'App\\Controller\\SecurityController::accueil'], [], [['text', '/accueil']], [], []],
        'snappy' => [[], ['_controller' => 'App\\Controller\\SnappyController::indexAction'], [], [['text', '/snappy']], [], []],
        'type_conge_index' => [[], ['_controller' => 'App\\Controller\\TypeCongeController::index'], [], [['text', '/type/conge/']], [], []],
        'type_conge_new' => [[], ['_controller' => 'App\\Controller\\TypeCongeController::new'], [], [['text', '/type/conge/new']], [], []],
        'type_conge_show' => [['id'], ['_controller' => 'App\\Controller\\TypeCongeController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/type/conge']], [], []],
        'type_conge_edit' => [['id'], ['_controller' => 'App\\Controller\\TypeCongeController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/type/conge']], [], []],
        'type_conge_delete' => [['id'], ['_controller' => 'App\\Controller\\TypeCongeController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/type/conge']], [], []],
        'type_permission_index' => [[], ['_controller' => 'App\\Controller\\TypePermissionController::index'], [], [['text', '/type/permission/']], [], []],
        'type_permission_new' => [[], ['_controller' => 'App\\Controller\\TypePermissionController::new'], [], [['text', '/type/permission/new']], [], []],
        'type_permission_show' => [['id'], ['_controller' => 'App\\Controller\\TypePermissionController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/type/permission']], [], []],
        'type_permission_edit' => [['id'], ['_controller' => 'App\\Controller\\TypePermissionController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/type/permission']], [], []],
        'type_permission_delete' => [['id'], ['_controller' => 'App\\Controller\\TypePermissionController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/type/permission']], [], []],
        'utilisateur_index' => [[], ['_controller' => 'App\\Controller\\UtilisateurController::index'], [], [['text', '/utilisateur/']], [], []],
        'utilisateur_accueil' => [[], ['_controller' => 'App\\Controller\\UtilisateurController::accueil'], [], [['text', '/utilisateur/accueil']], [], []],
        'utilisateur_new' => [[], ['_controller' => 'App\\Controller\\UtilisateurController::new'], [], [['text', '/utilisateur/new']], [], []],
        'utilisateur_show' => [['id'], ['_controller' => 'App\\Controller\\UtilisateurController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/utilisateur']], [], []],
        'utilisateur_edit' => [['id'], ['_controller' => 'App\\Controller\\UtilisateurController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/utilisateur']], [], []],
        'utilisateur_delete' => [['id'], ['_controller' => 'App\\Controller\\UtilisateurController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/utilisateur']], [], []],
        'fos_js_routing_js' => [['_format'], ['_controller' => 'fos_js_routing.controller:indexAction', '_format' => 'js'], ['_format' => 'js|json'], [['variable', '.', 'js|json', '_format', true], ['text', '/js/routing']], [], []],
        'fullcalendar_load_events' => [[], ['_controller' => 'Toiba\\FullCalendarBundle\\Controller\\CalendarController::loadAction'], [], [['text', '/fc-load-events']], [], []],
        '_twig_error_test' => [['code', '_format'], ['_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code' => '\\d+'], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '\\d+', 'code', true], ['text', '/_error']], [], []],
        '_wdt' => [['token'], ['_controller' => 'web_profiler.controller.profiler::toolbarAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_wdt']], [], []],
        '_profiler_home' => [[], ['_controller' => 'web_profiler.controller.profiler::homeAction'], [], [['text', '/_profiler/']], [], []],
        '_profiler_search' => [[], ['_controller' => 'web_profiler.controller.profiler::searchAction'], [], [['text', '/_profiler/search']], [], []],
        '_profiler_search_bar' => [[], ['_controller' => 'web_profiler.controller.profiler::searchBarAction'], [], [['text', '/_profiler/search_bar']], [], []],
        '_profiler_phpinfo' => [[], ['_controller' => 'web_profiler.controller.profiler::phpinfoAction'], [], [['text', '/_profiler/phpinfo']], [], []],
        '_profiler_search_results' => [['token'], ['_controller' => 'web_profiler.controller.profiler::searchResultsAction'], [], [['text', '/search/results'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_open_file' => [[], ['_controller' => 'web_profiler.controller.profiler::openAction'], [], [['text', '/_profiler/open']], [], []],
        '_profiler' => [['token'], ['_controller' => 'web_profiler.controller.profiler::panelAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_router' => [['token'], ['_controller' => 'web_profiler.controller.router::panelAction'], [], [['text', '/router'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_exception' => [['token'], ['_controller' => 'web_profiler.controller.exception::showAction'], [], [['text', '/exception'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_exception_css' => [['token'], ['_controller' => 'web_profiler.controller.exception::cssAction'], [], [['text', '/exception.css'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        'logout' => [[], [], [], [['text', '/logout']], [], []],
    ];
        }
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && null !== $name) {
            do {
                if ((self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
                    unset($parameters['_locale']);
                    $name .= '.'.$locale;
                    break;
                }
            } while (false !== $locale = strstr($locale, '_', true));
        }

        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
