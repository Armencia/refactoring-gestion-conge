<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login.html.twig */
class __TwigTemplate_4b04553a8c115c25cfb573ae7f458bcdabfdc522478b92ac1252780413da6587 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<!--
\tMassively by HTML5 UP
\thtml5up.net | @ajlkn
\tFree for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
  <title>Massively by HTML5 UP</title>
  <meta charset=\"utf-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\" />
  <link rel=\"stylesheet\" href=\"/css/main.css\" />
  <noscript><link rel=\"stylesheet\" href=\"/css/noscript.css\" /></noscript>
</head>
<body class=\"is-preload\">

<!-- Wrapper -->
<div id=\"wrapper\" class=\"fade-in\">

  <!-- Intro -->
  <div id=\"intro\">
    <h1>Maitrisez la gestion de vos<br />
      Congé et Permission</h1>

    <ul class=\"actions\">
      <li><a href=\"#header\" class=\"button icon solo fa-arrow-down scrolly\">Continue</a></li>
    </ul>
  </div>

  <!-- Header -->
  <header id=\"header\">
    <a href=\"index.log\" class=\"logo\">Connectez-Vous</a>
  </header>

  <!-- Nav -->
  <nav id=\"nav\">
    <ul class=\"links\">
      <li class=\"active\"><a>Se connecter</a></li>
    </ul>
  </nav>

  <!-- Main -->
  <div id=\"main\">

    <!-- Featured Post -->
    <article class=\"post featured\">
      <header class=\"major\">
        <img src=\"/img/logokraoma.jpg\" style=\"width:200px;\">

        <form action=\"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("verify_login");
        echo "\" method=\"POST\">
          <div class=\"input-group mb-3\">
            <input type=\"text\" id=\"username\" name=\"_username\" class=\"form-control input_user\" value=\"\" placeholder=\"entrez votre nom d'utilisateur\"  required>
          </div>
          <br>
          <div class=\"input-group mb-2\">
            <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control input_pass\" value=\"\" placeholder=\"entrez votre mot de passe\" required>
          </div>
          <br>
          <div class=\"d-flex justify-content-center mt-3 login_container\">
            <button type=\"submit\" name=\"button\" class=\"btn login_btn\">Se connecter</button>
          </div>
        </form>
      </header>
    </article>
  </div>
</div>

<!-- Scripts -->
<script src=\"/js/jquery.min.js\"></script>
<script src=\"/js/jquery.scrollex.min.js\"></script>
<script src=\"/js/jquery.scrolly.min.js\"></script>
<script src=\"/js/browser.min.js\"></script>
<script src=\"/js/breakpoints.min.js\"></script>
<script src=\"/js/util.js\"></script>
<script src=\"/js/main.js\"></script>

</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 50,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE HTML>
<!--
\tMassively by HTML5 UP
\thtml5up.net | @ajlkn
\tFree for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
  <title>Massively by HTML5 UP</title>
  <meta charset=\"utf-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\" />
  <link rel=\"stylesheet\" href=\"/css/main.css\" />
  <noscript><link rel=\"stylesheet\" href=\"/css/noscript.css\" /></noscript>
</head>
<body class=\"is-preload\">

<!-- Wrapper -->
<div id=\"wrapper\" class=\"fade-in\">

  <!-- Intro -->
  <div id=\"intro\">
    <h1>Maitrisez la gestion de vos<br />
      Congé et Permission</h1>

    <ul class=\"actions\">
      <li><a href=\"#header\" class=\"button icon solo fa-arrow-down scrolly\">Continue</a></li>
    </ul>
  </div>

  <!-- Header -->
  <header id=\"header\">
    <a href=\"index.log\" class=\"logo\">Connectez-Vous</a>
  </header>

  <!-- Nav -->
  <nav id=\"nav\">
    <ul class=\"links\">
      <li class=\"active\"><a>Se connecter</a></li>
    </ul>
  </nav>

  <!-- Main -->
  <div id=\"main\">

    <!-- Featured Post -->
    <article class=\"post featured\">
      <header class=\"major\">
        <img src=\"/img/logokraoma.jpg\" style=\"width:200px;\">

        <form action=\"{{ path('verify_login') }}\" method=\"POST\">
          <div class=\"input-group mb-3\">
            <input type=\"text\" id=\"username\" name=\"_username\" class=\"form-control input_user\" value=\"\" placeholder=\"entrez votre nom d'utilisateur\"  required>
          </div>
          <br>
          <div class=\"input-group mb-2\">
            <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control input_pass\" value=\"\" placeholder=\"entrez votre mot de passe\" required>
          </div>
          <br>
          <div class=\"d-flex justify-content-center mt-3 login_container\">
            <button type=\"submit\" name=\"button\" class=\"btn login_btn\">Se connecter</button>
          </div>
        </form>
      </header>
    </article>
  </div>
</div>

<!-- Scripts -->
<script src=\"/js/jquery.min.js\"></script>
<script src=\"/js/jquery.scrollex.min.js\"></script>
<script src=\"/js/jquery.scrolly.min.js\"></script>
<script src=\"/js/browser.min.js\"></script>
<script src=\"/js/breakpoints.min.js\"></script>
<script src=\"/js/util.js\"></script>
<script src=\"/js/main.js\"></script>

</body>
</html>", "security/login.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\security\\login.html.twig");
    }
}
