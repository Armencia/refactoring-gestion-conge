<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/menu.html.twig */
class __TwigTemplate_c5cd6f4a0125bab145f3b016b98e03d1c9647964663e4b786ab48aba56851700 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/menu.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/menu.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    <link href=\"https://stackpath.bootstrapcdn.com/bootswatch/4.2.1/flatly/bootstrap.min.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"/css/css.css\">
    ";
        // line 9
        echo "


    ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 32
        echo "    ";
        if (((isset($context["role"]) || array_key_exists("role", $context) ? $context["role"] : (function () { throw new RuntimeError('Variable "role" does not exist.', 32, $this->source); })()) == true)) {
            // line 33
            echo "<body>
    <nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">
        <div class=\"container\">

        <br><br>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor02\" aria-controls=\"navbarColor02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>

        <div class=\"collapse navbar-collapse\" id=\"navbarColor02\">
            <ul class=\"navbar-nav mr-auto\">
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"";
            // line 45
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("apropos");
            echo "\">A propos <span class=\"sr-only\"></span></a>
                </li>


                <!--<li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"";
            // line 50
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("booking_index");
            echo "\">Planning</a>
                </li>
                -->

                <li class=\"dropdown\">
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Congé
                    </a>

                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                        <a class=\"dropdown-item\" href=\"";
            // line 60
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("indexCongeRecu");
            echo "\"><img src=\"/img/email.png\" style=\"width: 40px; height: auto;\">Demande Congés Reçues</a>


                    </div>
                </li>

                <li class=\"dropdown\">
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Permission
                    </a>

                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                        <a class=\"dropdown-item\" href=\"";
            // line 72
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("indexPermissionRecu");
            echo "\"><img src=\"/img/email.png\" style=\"width: 40px; height: auto;\">Demande Permission Reçues</a>


                    </div>
                </li>


                <li class=\"dropdown\">
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Gerer
                    </a>

                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                        <a class=\"dropdown-item\" href=\"";
            // line 85
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateur_index");
            echo "\">Utilisateur</a>
                        <a class=\"dropdown-item\" href=\"";
            // line 86
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("medaille_index");
            echo "\">Medaille</a>
                        <a class=\"dropdown-item\" href=\"";
            // line 87
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("departement_index");
            echo "\">Departement</a>
                        <a class=\"dropdown-item\" href=\"";
            // line 88
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("type_conge_index");
            echo "\">Type de Congé</a>
                        <a class=\"dropdown-item\" href=\"";
            // line 89
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("type_permission_index");
            echo "\">Type de Permission</a>


                    </div>
                </li>

            </ul>
            <ul class=\"navbar-nav \">
                <li class=\"dropdown\">
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <img src=\"/img/menu.png\" style=\"width: 40px;\">
                    </a>

                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                        <a class=\"dropdown-item\" href=\"";
            // line 103
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil");
            echo "\">Profil</a>
                        <a class=\"dropdown-item\" href=\"";
            // line 104
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
            echo "\">Deconnexion</a>


                    </div>
                </li>
            </ul>
        </div>
        </div>

    </nav>


";
        } else {
            // line 117
            echo "
    <nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">
        <div class=\"container\">
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor02\" aria-controls=\"navbarColor02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
            </button>

            <div class=\"collapse navbar-collapse\" id=\"navbarColor02\">
                <ul class=\"navbar-nav mr-auto\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"";
            // line 127
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("apropos");
            echo "\">A propos <span class=\"sr-only\">(current)</span></a>
                    </li>
                    <li class=\"dropdown\">
                        <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            Conge
                        </a>

                        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                            <a class=\"dropdown-item\" href=\"";
            // line 135
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("conge_new");
            echo "\">Nouveau Congé</a>
                            <a class=\"dropdown-item\" href=\"";
            // line 136
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("conge_index");
            echo "\">Congé Enregistrer</a>
                            <a class=\"dropdown-item\" href=\"";
            // line 137
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("indexCogeEnvoyer");
            echo "\">Congé Envoyé</a>


                        </div>
                    </li>
                    <li class=\"dropdown\">
                        <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            Permission
                        </a>

                        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                            <a class=\"dropdown-item\" href=\"";
            // line 148
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("permission_new");
            echo "\">Nouveau Permission</a>
                            <a class=\"dropdown-item\" href=\"";
            // line 149
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("permission_index");
            echo "\">Permission Enregistré</a>
                            <a class=\"dropdown-item\" href=\"";
            // line 150
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("indexPermissionEnvoyer");
            echo "\">Permission Envoyé</a>


                        </div>
                    </li>
                    <!--
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"/booking\">Planning</a>
                    </li>
                    -->
                    
                </ul>
                <ul class=\"navbar-nav mr-auto offset-6\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"";
            // line 164
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logouta");
            echo "\">Se déconnecter</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

";
        }
        // line 172
        echo "</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "        ";
        // line 14
        echo "        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.cs\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("fontawesome/css/fontawesome.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("fontawesome/css/fontawesome.min.css"), "html", null, true);
        echo "\" />
        <script type=\"text/javascript\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("fontawesome/js/fontawesome.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("fontawesome/js/fontawesome.min.js"), "html", null, true);
        echo "\"></script>


        ";
        // line 22
        echo "        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
        <script src=\"https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js\"></script>
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js\"></script>
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("datetimepicker/css/bootstrap-datetimepicker.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("datetimepicker/css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\" />
        <script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("datetimepicker/js/bootstrap-datetimepicker.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("datetimepicker/js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  346 => 30,  342 => 29,  338 => 28,  334 => 27,  327 => 22,  321 => 18,  317 => 17,  313 => 16,  309 => 15,  306 => 14,  304 => 13,  295 => 12,  277 => 5,  265 => 172,  254 => 164,  237 => 150,  233 => 149,  229 => 148,  215 => 137,  211 => 136,  207 => 135,  196 => 127,  184 => 117,  168 => 104,  164 => 103,  147 => 89,  143 => 88,  139 => 87,  135 => 86,  131 => 85,  115 => 72,  100 => 60,  87 => 50,  79 => 45,  65 => 33,  62 => 32,  60 => 12,  55 => 9,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <title>{% block title %}Welcome!{% endblock %}</title>
    <link href=\"https://stackpath.bootstrapcdn.com/bootswatch/4.2.1/flatly/bootstrap.min.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"/css/css.css\">
    {#<link rel=\"stylesheet\" href=\"{{ asset('/css/bootswatch.min.css') }}\">#}



    {% block stylesheets %}
        {# FONT AWESOME#}
        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.cs\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"{{ asset('fontawesome/css/fontawesome.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('fontawesome/css/fontawesome.min.css') }}\" />
        <script type=\"text/javascript\" src=\"{{ asset('fontawesome/js/fontawesome.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('fontawesome/js/fontawesome.min.js') }}\"></script>


        {# Toiba Full calendar #}
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
        <script src=\"https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js\"></script>
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js\"></script>
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css\">
        <link rel=\"stylesheet\" href=\"{{ asset('datetimepicker/css/bootstrap-datetimepicker.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('datetimepicker/css/bootstrap-datetimepicker.min.css') }}\" />
        <script type=\"text/javascript\" src=\"{{ asset('datetimepicker/js/bootstrap-datetimepicker.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('datetimepicker/js/bootstrap-datetimepicker.min.js') }}\"></script>
    {% endblock %}
    {% if role == true %}
<body>
    <nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">
        <div class=\"container\">

        <br><br>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor02\" aria-controls=\"navbarColor02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"></span>
        </button>

        <div class=\"collapse navbar-collapse\" id=\"navbarColor02\">
            <ul class=\"navbar-nav mr-auto\">
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"{{ path('apropos') }}\">A propos <span class=\"sr-only\"></span></a>
                </li>


                <!--<li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"{{ path('booking_index') }}\">Planning</a>
                </li>
                -->

                <li class=\"dropdown\">
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Congé
                    </a>

                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                        <a class=\"dropdown-item\" href=\"{{ path('indexCongeRecu') }}\"><img src=\"/img/email.png\" style=\"width: 40px; height: auto;\">Demande Congés Reçues</a>


                    </div>
                </li>

                <li class=\"dropdown\">
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Permission
                    </a>

                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                        <a class=\"dropdown-item\" href=\"{{ path('indexPermissionRecu') }}\"><img src=\"/img/email.png\" style=\"width: 40px; height: auto;\">Demande Permission Reçues</a>


                    </div>
                </li>


                <li class=\"dropdown\">
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Gerer
                    </a>

                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                        <a class=\"dropdown-item\" href=\"{{ path('utilisateur_index') }}\">Utilisateur</a>
                        <a class=\"dropdown-item\" href=\"{{ path('medaille_index') }}\">Medaille</a>
                        <a class=\"dropdown-item\" href=\"{{ path('departement_index') }}\">Departement</a>
                        <a class=\"dropdown-item\" href=\"{{ path('type_conge_index') }}\">Type de Congé</a>
                        <a class=\"dropdown-item\" href=\"{{ path('type_permission_index') }}\">Type de Permission</a>


                    </div>
                </li>

            </ul>
            <ul class=\"navbar-nav \">
                <li class=\"dropdown\">
                    <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        <img src=\"/img/menu.png\" style=\"width: 40px;\">
                    </a>

                    <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                        <a class=\"dropdown-item\" href=\"{{ path('profil') }}\">Profil</a>
                        <a class=\"dropdown-item\" href=\"{{ path('logout') }}\">Deconnexion</a>


                    </div>
                </li>
            </ul>
        </div>
        </div>

    </nav>


{% else %}

    <nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">
        <div class=\"container\">
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor02\" aria-controls=\"navbarColor02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
            </button>

            <div class=\"collapse navbar-collapse\" id=\"navbarColor02\">
                <ul class=\"navbar-nav mr-auto\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"{{ path('apropos') }}\">A propos <span class=\"sr-only\">(current)</span></a>
                    </li>
                    <li class=\"dropdown\">
                        <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            Conge
                        </a>

                        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                            <a class=\"dropdown-item\" href=\"{{ path('conge_new') }}\">Nouveau Congé</a>
                            <a class=\"dropdown-item\" href=\"{{ path('conge_index') }}\">Congé Enregistrer</a>
                            <a class=\"dropdown-item\" href=\"{{ path('indexCogeEnvoyer') }}\">Congé Envoyé</a>


                        </div>
                    </li>
                    <li class=\"dropdown\">
                        <a class=\"btn btn-secondary dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            Permission
                        </a>

                        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink\">
                            <a class=\"dropdown-item\" href=\"{{ path('permission_new') }}\">Nouveau Permission</a>
                            <a class=\"dropdown-item\" href=\"{{ path('permission_index') }}\">Permission Enregistré</a>
                            <a class=\"dropdown-item\" href=\"{{ path('indexPermissionEnvoyer') }}\">Permission Envoyé</a>


                        </div>
                    </li>
                    <!--
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"/booking\">Planning</a>
                    </li>
                    -->
                    
                </ul>
                <ul class=\"navbar-nav mr-auto offset-6\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"{{ path ('logouta') }}\">Se déconnecter</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

{% endif %}
</body>
</html>
", "home/menu.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\home\\menu.html.twig");
    }
}
