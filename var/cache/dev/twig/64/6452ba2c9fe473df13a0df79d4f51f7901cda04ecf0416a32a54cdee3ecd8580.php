<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* gerer/index.html.twig */
class __TwigTemplate_5deefcef77de00a391ab766542c19d91cf7fecc2d887f4587129ec992b20335a extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "gerer/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "gerer/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "gerer/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Tableau de bord";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "</br></br>


<div class=\"row justify-content-around\">
   <div class=\"col-3\">
    <div class=\"card bg-light mb-3\" style=\"max-width: 20rem;\">
        <div class=\"card-header\"><a href= \"/departement\">Départements</a></div>
            <div class=\"card-body\">
              <h4 class=\"card-title\">Light card title</h4>
              <p class=\"card-text\"></p>
            </div>
    </div>
  </div>
   <div class=\"col-3\">
    <div class=\"card text-white bg-success mb-3\" style=\"max-width: 20rem;\">
      <div class=\"card-header\"><a href= \"/role\">Rôles</a></div>
        <div class=\"card-body\">
          <h4 class=\"card-title\">Secondary card title</h4>
          <p class=\"card-text\"></p>
        </div>
    </div>
  </div>
  <div class=\"col-3\">
    <div class=\"card text-white bg-secondary mb-3\" style=\"max-width: 20rem;\">
      <div class=\"card-header\"><a href= \"/utilisateur\">Utilisateurs</a></div>
        <div class=\"card-body\">
          <h4 class=\"card-title\">Success card title</h4>
          <p class=\"card-text\"></p>
        </div>
    </div>
  </div>
</div>

</br>
<div class=\"row justify-content-around\">
   <div class=\"col-3\">
      <div class=\"card text-white bg-danger mb-3\" style=\"max-width: 20rem;\">
        <div class=\"card-header\"><a href= \"/medaille\">Médaille</a></div>
          <div class=\"card-body\">
            <h4 class=\"card-title\">Danger card title</h4>
            <p class=\"card-text\"></p>
          </div>
      </div>
  </div>
  <div class=\"col-3\">
      <div class=\"card text-white bg-warning mb-3\" style=\"max-width: 20rem;\">
        <div class=\"card-header\"><a href= \"/conge/\">Type de congé</a></div>
          <div class=\"card-body\">
            <h4 class=\"card-title\">Warning card title</h4>
            <p class=\"card-text\"></p>
          </div>
      </div>
 </div>

 <div class=\"col-3\">
    <div class=\"card text-white bg-info mb-3\" style=\"max-width: 20rem;\">
      <div class=\"card-header\"><a href= \"/departement\">Catégorie des types de demande</a></div>
        <div class=\"card-body\">
          <h4 class=\"card-title\">Info card title</h4>
          <p class=\"card-text\"></p>
        </div>
    </div>
  </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "gerer/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 6,  75 => 5,  57 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Tableau de bord{% endblock %}

{% block body %}
</br></br>


<div class=\"row justify-content-around\">
   <div class=\"col-3\">
    <div class=\"card bg-light mb-3\" style=\"max-width: 20rem;\">
        <div class=\"card-header\"><a href= \"/departement\">Départements</a></div>
            <div class=\"card-body\">
              <h4 class=\"card-title\">Light card title</h4>
              <p class=\"card-text\"></p>
            </div>
    </div>
  </div>
   <div class=\"col-3\">
    <div class=\"card text-white bg-success mb-3\" style=\"max-width: 20rem;\">
      <div class=\"card-header\"><a href= \"/role\">Rôles</a></div>
        <div class=\"card-body\">
          <h4 class=\"card-title\">Secondary card title</h4>
          <p class=\"card-text\"></p>
        </div>
    </div>
  </div>
  <div class=\"col-3\">
    <div class=\"card text-white bg-secondary mb-3\" style=\"max-width: 20rem;\">
      <div class=\"card-header\"><a href= \"/utilisateur\">Utilisateurs</a></div>
        <div class=\"card-body\">
          <h4 class=\"card-title\">Success card title</h4>
          <p class=\"card-text\"></p>
        </div>
    </div>
  </div>
</div>

</br>
<div class=\"row justify-content-around\">
   <div class=\"col-3\">
      <div class=\"card text-white bg-danger mb-3\" style=\"max-width: 20rem;\">
        <div class=\"card-header\"><a href= \"/medaille\">Médaille</a></div>
          <div class=\"card-body\">
            <h4 class=\"card-title\">Danger card title</h4>
            <p class=\"card-text\"></p>
          </div>
      </div>
  </div>
  <div class=\"col-3\">
      <div class=\"card text-white bg-warning mb-3\" style=\"max-width: 20rem;\">
        <div class=\"card-header\"><a href= \"/conge/\">Type de congé</a></div>
          <div class=\"card-body\">
            <h4 class=\"card-title\">Warning card title</h4>
            <p class=\"card-text\"></p>
          </div>
      </div>
 </div>

 <div class=\"col-3\">
    <div class=\"card text-white bg-info mb-3\" style=\"max-width: 20rem;\">
      <div class=\"card-header\"><a href= \"/departement\">Catégorie des types de demande</a></div>
        <div class=\"card-body\">
          <h4 class=\"card-title\">Info card title</h4>
          <p class=\"card-text\"></p>
        </div>
    </div>
  </div>
</div>

{% endblock %}
", "gerer/index.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\gerer\\index.html.twig");
    }
}
