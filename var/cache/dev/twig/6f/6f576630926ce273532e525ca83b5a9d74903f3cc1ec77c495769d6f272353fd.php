<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/profil.html.twig */
class __TwigTemplate_89f2d50677318f895a0b29c50cd89dbf68d361a683998071ba35dd92be2ffe35 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/profil.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/profil.html.twig"));

        // line 2
        $context["NbRestant"] = 0;
        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "home/profil.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <div class=\"container\">
        <b><h1>Votre Profil</h1></b><br>
        <div class=\"row\">
            <div class=\"col col-4 offset-2\">
                ";
        // line 10
        if (twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 10, $this->source); })()), "filename", [], "any", false, false, false, 10)) {
            // line 11
            echo "                    ";
            // line 12
            echo "                    <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset((isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 12, $this->source); })()), "imageFile"), "html", null, true);
            echo "\" alt=\"\" style=\"width: 300px; height: auto\">
                ";
        } else {
            // line 14
            echo "                    <img src=\"/images/properties/user.png\" alt=\"\" style=\"width: 300px; height: auto\"><br><br><br>
                ";
        }
        // line 16
        echo "                <br>
            </div>
            <div class=\"col col-5\"><br><br><br><br>
                <h5><div> <b>N° Matricule</b> :";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 19, $this->source); })()), "matricule", [], "any", false, false, false, 19), "html", null, true);
        echo "</div>
                <div> <b>Username</b> :";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 20, $this->source); })()), "username", [], "any", false, false, false, 20), "html", null, true);
        echo "</div>
                <div> <b>Nom</b> :";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 21, $this->source); })()), "nom", [], "any", false, false, false, 21), "html", null, true);
        echo "</div>
                <div> <b>Prénom</b> :";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 22, $this->source); })()), "prenom", [], "any", false, false, false, 22), "html", null, true);
        echo "</div>
                <div> <b>Email</b> :";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 23, $this->source); })()), "email", [], "any", false, false, false, 23), "html", null, true);
        echo "</div>
                <div> <b>CIN N°</b> :";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 24, $this->source); })()), "CIN", [], "any", false, false, false, 24), "html", null, true);
        echo "</div>
                <div> <b>Medaille</b> :";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 25, $this->source); })()), "medaille", [], "any", false, false, false, 25), "nom", [], "any", false, false, false, 25), "html", null, true);
        echo "</div>
                <div> <b>Département</b> :";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 26, $this->source); })()), "departement", [], "any", false, false, false, 26), "nom", [], "any", false, false, false, 26), "html", null, true);
        echo "</div>
                <div> <b>Nombre de Jour Restant</b> :";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 27, $this->source); })()), "nbJour", [], "any", false, false, false, 27), "html", null, true);
        echo "</div>
                <br></h5>
                <br>
            </div>

            <div class=\"btn btn-primary offset-3\">
                <a href=\"#\">Modifer Mon Profil</a>
            </div>
        </div>
    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/profil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 27,  122 => 26,  118 => 25,  114 => 24,  110 => 23,  106 => 22,  102 => 21,  98 => 20,  94 => 19,  89 => 16,  85 => 14,  79 => 12,  77 => 11,  75 => 10,  68 => 5,  59 => 4,  48 => 1,  46 => 2,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% set NbRestant = 0 %}

{% block body %}

    <div class=\"container\">
        <b><h1>Votre Profil</h1></b><br>
        <div class=\"row\">
            <div class=\"col col-4 offset-2\">
                {% if user.filename %}
                    {#<img src=\"/images/properties/{{ materiel.filename }}\" alt=\"\" style=\"width: 400px; height: auto\">#}
                    <img src=\"{{ vich_uploader_asset(user,'imageFile') }}\" alt=\"\" style=\"width: 300px; height: auto\">
                {% else %}
                    <img src=\"/images/properties/user.png\" alt=\"\" style=\"width: 300px; height: auto\"><br><br><br>
                {% endif %}
                <br>
            </div>
            <div class=\"col col-5\"><br><br><br><br>
                <h5><div> <b>N° Matricule</b> :{{ user.matricule }}</div>
                <div> <b>Username</b> :{{ user.username }}</div>
                <div> <b>Nom</b> :{{ user.nom }}</div>
                <div> <b>Prénom</b> :{{ user.prenom }}</div>
                <div> <b>Email</b> :{{ user.email }}</div>
                <div> <b>CIN N°</b> :{{ user.CIN }}</div>
                <div> <b>Medaille</b> :{{ user.medaille.nom }}</div>
                <div> <b>Département</b> :{{ user.departement.nom }}</div>
                <div> <b>Nombre de Jour Restant</b> :{{ user.nbJour }}</div>
                <br></h5>
                <br>
            </div>

            <div class=\"btn btn-primary offset-3\">
                <a href=\"#\">Modifer Mon Profil</a>
            </div>
        </div>
    </div>


{% endblock %}", "home/profil.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\home\\profil.html.twig");
    }
}
