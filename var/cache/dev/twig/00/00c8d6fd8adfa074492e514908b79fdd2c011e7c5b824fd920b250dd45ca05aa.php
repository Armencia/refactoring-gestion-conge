<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/accueil.html.twig */
class __TwigTemplate_54bf6f5549a8aa238a8eabad4dab217e93e7ba7cb8fcde3250b74813c7ad9584 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/accueil.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/accueil.html.twig"));

        // line 1
        echo "<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">
<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js\"></script>
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>

<head>
  <title>My Awesome Login Page</title>
  <link rel=\"stylesheet\" href=\"../css/log.css\">
  <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.1/css/all.css\" integrity=\"sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP\" crossorigin=\"anonymous\">
</head>
<!--Coded with love by Mutiullah Samim-->
<body>
<div class=\"container h-100\">
  <div class=\"d-flex justify-content-center h-100\">
    <div class=\"user_card\">
      <div class=\"d-flex justify-content-center\">
        <div class=\"brand_logo_container\">
          <img src=\"../img/logokraoma.jpg\" class=\"brand_logo\" alt=\"Logo\">
        </div>
      </div>
      <div class=\"d-flex justify-content-center form_container\">
        <form action=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("verify_login");
        echo "\" method=\"POST\">
          <div class=\"input-group mb-3\">
            <div class=\"input-group-append\">
              <span class=\"input-group-text\"><i class=\"fas fa-user\"></i></span>
            </div>
            <input type=\"text\" id=\"username\" name=\"_username\" class=\"form-control input_user\" value=\"\" placeholder=\"username\"  required>
          </div>
          <div class=\"input-group mb-2\">
            <div class=\"input-group-append\">
              <span class=\"input-group-text\"><i class=\"fas fa-key\"></i></span>
            </div>
            <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control input_pass\" value=\"\" placeholder=\"password\" required>
          </div>
          <div class=\"form-group\">
            <div class=\"custom-control custom-checkbox\">
              <input type=\"checkbox\" class=\"custom-control-input\" id=\"customControlInline\">
              <label class=\"custom-control-label\" for=\"customControlInline\">Remember me</label>
            </div>
          </div>
          <div class=\"d-flex justify-content-center mt-3 login_container\">
            <button type=\"submit\" name=\"button\" class=\"btn login_btn\">Login</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "home/accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 27,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">
<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js\"></script>
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>

<head>
  <title>My Awesome Login Page</title>
  <link rel=\"stylesheet\" href=\"../css/log.css\">
  <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.1/css/all.css\" integrity=\"sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP\" crossorigin=\"anonymous\">
</head>
<!--Coded with love by Mutiullah Samim-->
<body>
<div class=\"container h-100\">
  <div class=\"d-flex justify-content-center h-100\">
    <div class=\"user_card\">
      <div class=\"d-flex justify-content-center\">
        <div class=\"brand_logo_container\">
          <img src=\"../img/logokraoma.jpg\" class=\"brand_logo\" alt=\"Logo\">
        </div>
      </div>
      <div class=\"d-flex justify-content-center form_container\">
        <form action=\"{{ path('verify_login') }}\" method=\"POST\">
          <div class=\"input-group mb-3\">
            <div class=\"input-group-append\">
              <span class=\"input-group-text\"><i class=\"fas fa-user\"></i></span>
            </div>
            <input type=\"text\" id=\"username\" name=\"_username\" class=\"form-control input_user\" value=\"\" placeholder=\"username\"  required>
          </div>
          <div class=\"input-group mb-2\">
            <div class=\"input-group-append\">
              <span class=\"input-group-text\"><i class=\"fas fa-key\"></i></span>
            </div>
            <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control input_pass\" value=\"\" placeholder=\"password\" required>
          </div>
          <div class=\"form-group\">
            <div class=\"custom-control custom-checkbox\">
              <input type=\"checkbox\" class=\"custom-control-input\" id=\"customControlInline\">
              <label class=\"custom-control-label\" for=\"customControlInline\">Remember me</label>
            </div>
          </div>
          <div class=\"d-flex justify-content-center mt-3 login_container\">
            <button type=\"submit\" name=\"button\" class=\"btn login_btn\">Login</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</body>
</html>
", "home/accueil.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\home\\accueil.html.twig");
    }
}
