<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* booking/index.html.twig */
class __TwigTemplate_479b5c1d771963497c236dc23213c4a6d8ee0ff390d482925717a1a2936d41e3 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "booking/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "booking/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "booking/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("booking_new");
        echo "\">Create new booking</a>

    ";
        // line 6
        $this->loadTemplate("@FullCalendar/Calendar/calendar.html.twig", "booking/index.html.twig", 6)->display($context);
        // line 7
        echo "
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\">Attribuer un Congé </h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <p>Modal body text goes here.</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" class=\"btn btn-primary\">Save changes</button>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 30
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 33
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 34
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://momentjs.com/downloads/moment.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale-all.js\"></script>
    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" ></script>
    <script type=\"text/javascript\">

        \$(function () {
            \$('#fullCalModal').on('shown.bs.modal', function () {
                \$('#myInput').trigger('focus')
            })
            \$('#calendar-holder').fullCalendar({

                locale: 'fr',
                header: {
                    left: 'prev, next, today',
                    center: 'title',
                    right: 'month, agendaWeek, agendaDay'
                },
                lazyFetching: true,
                navLinks: true,
                editable:true,
                selectable:true,
                eventSources: [
                    {
                        url: \"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fullcalendar_load_events");
        echo "\",
                        type: 'POST',
                        data:  {
                            filters: {}
                        },
                        error: function () {
                            alert('There was an error while fetching FullCalendar!');
                        }
                    }
                ],
                dayClick:  function(event, jsEvent, view) {
                    endtime = \$.fullCalendar.moment(event.end).format('H:mm');
                    starttime = \$.fullCalendar.moment(event.start).format('dddd, Do MMMM  YYYY, H:mm');
                    isAllDay = \$.fullCalendar.moment(event.start).format('dddd, Do MMMM  YYYY');

                    var mywhen = starttime + ' - ' + endtime;
                    console.log(event);
                    console.log(event.commentaire);
                    console.log('avant modal');
                    \$('.modal').modal();

                },
                dayRender: function (date, element, view)
                {
                    var date = new Date(date);
                    var day = date.getDate().toString();
                    if (day.length == 1)
                        day = 0 + day;
                    var year = date.getFullYear();
                    var month = (date.getMonth() + 1).toString();
                    if (month.length == 1)
                        month = 0 + month;
                    var dateStr = year + \"-\" + month + \"-\" + day ;

                    if ( dateStr.toString() == '2019-03-11')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-12')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                    if ( dateStr.toString() == '2019-03-13')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-14')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-15')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-17')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-18')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                    if ( dateStr.toString() == '2019-03-19')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-20')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-21')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-22')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-16')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }


                    if ( dateStr.toString() == '2019-04-15')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-04-16')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-04-17')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                    if ( dateStr.toString() == '2019-04-18')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                    if ( dateStr.toString() == '2019-04-19')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                },
            });


        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "booking/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 60,  134 => 34,  125 => 33,  114 => 30,  105 => 29,  75 => 7,  73 => 6,  67 => 4,  58 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <a href=\"{{ path('booking_new') }}\">Create new booking</a>

    {% include '@FullCalendar/Calendar/calendar.html.twig' %}

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\">Attribuer un Congé </h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <p>Modal body text goes here.</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" class=\"btn btn-primary\">Save changes</button>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{% block stylesheets %}
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css\">
{% endblock %}

{% block javascripts %}
    {{ parent() }}
    <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://momentjs.com/downloads/moment.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale-all.js\"></script>
    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" ></script>
    <script type=\"text/javascript\">

        \$(function () {
            \$('#fullCalModal').on('shown.bs.modal', function () {
                \$('#myInput').trigger('focus')
            })
            \$('#calendar-holder').fullCalendar({

                locale: 'fr',
                header: {
                    left: 'prev, next, today',
                    center: 'title',
                    right: 'month, agendaWeek, agendaDay'
                },
                lazyFetching: true,
                navLinks: true,
                editable:true,
                selectable:true,
                eventSources: [
                    {
                        url: \"{{ path('fullcalendar_load_events') }}\",
                        type: 'POST',
                        data:  {
                            filters: {}
                        },
                        error: function () {
                            alert('There was an error while fetching FullCalendar!');
                        }
                    }
                ],
                dayClick:  function(event, jsEvent, view) {
                    endtime = \$.fullCalendar.moment(event.end).format('H:mm');
                    starttime = \$.fullCalendar.moment(event.start).format('dddd, Do MMMM  YYYY, H:mm');
                    isAllDay = \$.fullCalendar.moment(event.start).format('dddd, Do MMMM  YYYY');

                    var mywhen = starttime + ' - ' + endtime;
                    console.log(event);
                    console.log(event.commentaire);
                    console.log('avant modal');
                    \$('.modal').modal();

                },
                dayRender: function (date, element, view)
                {
                    var date = new Date(date);
                    var day = date.getDate().toString();
                    if (day.length == 1)
                        day = 0 + day;
                    var year = date.getFullYear();
                    var month = (date.getMonth() + 1).toString();
                    if (month.length == 1)
                        month = 0 + month;
                    var dateStr = year + \"-\" + month + \"-\" + day ;

                    if ( dateStr.toString() == '2019-03-11')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-12')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                    if ( dateStr.toString() == '2019-03-13')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-14')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-15')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-17')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-18')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                    if ( dateStr.toString() == '2019-03-19')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-20')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-21')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-22')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-03-16')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#ff85d1;font-size: 14px;'>Rakoto Fotsy Vacances</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"#ff85d1\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }


                    if ( dateStr.toString() == '2019-04-15')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-04-16')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }

                    if ( dateStr.toString() == '2019-04-17')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                    if ( dateStr.toString() == '2019-04-18')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                    if ( dateStr.toString() == '2019-04-19')
                    {
                        var new_description =  \"<div align='center'><br><br><br><br><span style='color:white;background-color:#1313ff;font-size: 14px;'>Rasoa Bakoly <br> Raison personnel</div>\";
                        element.append(new_description);
                        \$(element).css(\"background\", \"blue\"); // it will set backgroud of that date
                        \$(element).css(\"title\", \"séjour groupe\"); // it will set backgroud of that date
                    }
                },
            });


        });
    </script>
{% endblock %}", "booking/index.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\booking\\index.html.twig");
    }
}
