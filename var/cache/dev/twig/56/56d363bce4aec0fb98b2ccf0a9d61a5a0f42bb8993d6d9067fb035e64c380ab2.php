<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* conge/pdf.html.twig */
class __TwigTemplate_983a3affaf10a7f00e1ea0fc80772edfa36b55b7c9f24af15a1d85b6618be328 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "conge/pdf.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "conge/pdf.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>
<body>
<div class=\"container\">
    <h1><center>Détails d'une demande de Conge</center></h1>

    <table class=\"table\">
        <tbody>
        <tr>
            <th>Date de demande</th>
            <td>";
        // line 18
        ((twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 18, $this->source); })()), "dateDemande", [], "any", false, false, false, 18)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 18, $this->source); })()), "dateDemande", [], "any", false, false, false, 18), "Y-m-d"), "html", null, true))) : (print ("")));
        echo "</td>
        </tr>
        <tr>
            <th>Matricule</th>
            <td>";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 22, $this->source); })()), "utilisateur", [], "any", false, false, false, 22), "matricule", [], "any", false, false, false, 22), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Nom</th>
            <td>";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 26, $this->source); })()), "utilisateur", [], "any", false, false, false, 26), "nom", [], "any", false, false, false, 26), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>CIN</th>
            <td>";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 30, $this->source); })()), "CIN", [], "any", false, false, false, 30), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 34, $this->source); })()), "email", [], "any", false, false, false, 34), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Raison</th>
            <td>";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 38, $this->source); })()), "raison", [], "any", false, false, false, 38), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Nombre de jours</th>
            <td>";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 42, $this->source); })()), "nombre", [], "any", false, false, false, 42), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Département</th>
            <td>";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 46, $this->source); })()), "utilisateur", [], "any", false, false, false, 46), "departement", [], "any", false, false, false, 46), "nom", [], "any", false, false, false, 46), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Type</th>
            <td>";
        // line 50
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 50, $this->source); })()), "typeConge", [], "any", false, false, false, 50), "nom", [], "any", false, false, false, 50), "html", null, true);
        echo "</td>
        </tr>

        <tr>
            <th>Date de début</th>
            <td>";
        // line 55
        ((twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 55, $this->source); })()), "dateDebut", [], "any", false, false, false, 55)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 55, $this->source); })()), "dateDebut", [], "any", false, false, false, 55)), "html", null, true))) : (print ("")));
        echo "</td>
        </tr>

        </tbody>
    </table>
</div>

</body>
</html>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "conge/pdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 55,  116 => 50,  109 => 46,  102 => 42,  95 => 38,  88 => 34,  81 => 30,  74 => 26,  67 => 22,  60 => 18,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>
<body>
<div class=\"container\">
    <h1><center>Détails d'une demande de Conge</center></h1>

    <table class=\"table\">
        <tbody>
        <tr>
            <th>Date de demande</th>
            <td>{{ conge.dateDemande ? conge.dateDemande|date('Y-m-d') }}</td>
        </tr>
        <tr>
            <th>Matricule</th>
            <td>{{ conge.utilisateur.matricule }}</td>
        </tr>
        <tr>
            <th>Nom</th>
            <td>{{ conge.utilisateur.nom }}</td>
        </tr>
        <tr>
            <th>CIN</th>
            <td>{{ conge.CIN }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ conge.email}}</td>
        </tr>
        <tr>
            <th>Raison</th>
            <td>{{ conge.raison }}</td>
        </tr>
        <tr>
            <th>Nombre de jours</th>
            <td>{{ conge.nombre }}</td>
        </tr>
        <tr>
            <th>Département</th>
            <td>{{  conge.utilisateur.departement.nom }}</td>
        </tr>
        <tr>
            <th>Type</th>
            <td>{{ conge.typeConge.nom }}</td>
        </tr>

        <tr>
            <th>Date de début</th>
            <td>{{ conge.dateDebut ? conge.dateDebut|date() }}</td>
        </tr>

        </tbody>
    </table>
</div>

</body>
</html>

", "conge/pdf.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\conge\\pdf.html.twig");
    }
}
