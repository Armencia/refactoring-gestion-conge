<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* permission/pdf.html.twig */
class __TwigTemplate_533812cb7225ac3750251268beb5001159b10dbf7c5fbb0fec17907aa2e9d1ec extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "permission/pdf.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "permission/pdf.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>
<body>
<div class=\"container\">
    <h1><center>Détails d'une demande de Permission</center></h1>


    <table class=\"table\">
        <tbody>
        <tr>
            <th>Date de demande</th>
            <td>";
        // line 19
        ((twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 19, $this->source); })()), "dateDemande", [], "any", false, false, false, 19)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 19, $this->source); })()), "dateDemande", [], "any", false, false, false, 19), "Y-m-d"), "html", null, true))) : (print ("")));
        echo "</td>
        </tr>
        <tr>
            <th>Matricule</th>
            <td>";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 23, $this->source); })()), "utilisateur", [], "any", false, false, false, 23), "matricule", [], "any", false, false, false, 23), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Nom</th>
            <td>";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 27, $this->source); })()), "utilisateur", [], "any", false, false, false, 27), "nom", [], "any", false, false, false, 27), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Prénoms</th>
            <td>";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 31, $this->source); })()), "utilisateur", [], "any", false, false, false, 31), "prenom", [], "any", false, false, false, 31), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>CIN</th>
            <td>";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 35, $this->source); })()), "CIN", [], "any", false, false, false, 35), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 39, $this->source); })()), "email", [], "any", false, false, false, 39), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Type</th>
            <td>";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 43, $this->source); })()), "typePermission", [], "any", false, false, false, 43), "nom", [], "any", false, false, false, 43), "html", null, true);
        echo "</td>
        </tr>
        ";
        // line 45
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 45, $this->source); })()), "typePermission", [], "any", false, false, false, 45), "nom", [], "any", false, false, false, 45) == "Autre")) {
            // line 46
            echo "            <tr>
                <th>Raison</th>
                <td>";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 48, $this->source); })()), "raison", [], "any", false, false, false, 48), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <th>nombre de Jour</th>
                <td>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 52, $this->source); })()), "nombre", [], "any", false, false, false, 52), "html", null, true);
            echo "</td>
            </tr>
        ";
        } else {
            // line 55
            echo "            <tr>
                <th>Nombre de Jour</th>
                <td>";
            // line 57
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 57, $this->source); })()), "typePermission", [], "any", false, false, false, 57), "NbJour", [], "any", false, false, false, 57), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        // line 60
        echo "
        <tr>
            <th>Département</th>
            <td>";
        // line 63
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 63, $this->source); })()), "utilisateur", [], "any", false, false, false, 63), "departement", [], "any", false, false, false, 63), "nom", [], "any", false, false, false, 63), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Date de début</th>
            <td>";
        // line 67
        ((twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 67, $this->source); })()), "dateDebut", [], "any", false, false, false, 67)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["permission"]) || array_key_exists("permission", $context) ? $context["permission"] : (function () { throw new RuntimeError('Variable "permission" does not exist.', 67, $this->source); })()), "dateDebut", [], "any", false, false, false, 67)), "html", null, true))) : (print ("")));
        echo "</td>
        </tr>

        </tbody>
    </table>
</div>

</body>
</html>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "permission/pdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 67,  142 => 63,  137 => 60,  131 => 57,  127 => 55,  121 => 52,  114 => 48,  110 => 46,  108 => 45,  103 => 43,  96 => 39,  89 => 35,  82 => 31,  75 => 27,  68 => 23,  61 => 19,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>
<body>
<div class=\"container\">
    <h1><center>Détails d'une demande de Permission</center></h1>


    <table class=\"table\">
        <tbody>
        <tr>
            <th>Date de demande</th>
            <td>{{ permission.dateDemande ? permission.dateDemande|date('Y-m-d') }}</td>
        </tr>
        <tr>
            <th>Matricule</th>
            <td>{{ permission.utilisateur.matricule }}</td>
        </tr>
        <tr>
            <th>Nom</th>
            <td>{{ permission.utilisateur.nom }}</td>
        </tr>
        <tr>
            <th>Prénoms</th>
            <td>{{ permission.utilisateur.prenom }}</td>
        </tr>
        <tr>
            <th>CIN</th>
            <td>{{ permission.CIN }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ permission.email}}</td>
        </tr>
        <tr>
            <th>Type</th>
            <td>{{ permission.typePermission.nom }}</td>
        </tr>
        {% if permission.typePermission.nom == 'Autre' %}
            <tr>
                <th>Raison</th>
                <td>{{ permission.raison }}</td>
            </tr>
            <tr>
                <th>nombre de Jour</th>
                <td>{{ permission.nombre }}</td>
            </tr>
        {% else %}
            <tr>
                <th>Nombre de Jour</th>
                <td>{{ permission.typePermission.NbJour }}</td>
            </tr>
        {% endif %}

        <tr>
            <th>Département</th>
            <td>{{  permission.utilisateur.departement.nom }}</td>
        </tr>
        <tr>
            <th>Date de début</th>
            <td>{{ permission.dateDebut ? permission.dateDebut|date() }}</td>
        </tr>

        </tbody>
    </table>
</div>

</body>
</html>

", "permission/pdf.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\permission\\pdf.html.twig");
    }
}
