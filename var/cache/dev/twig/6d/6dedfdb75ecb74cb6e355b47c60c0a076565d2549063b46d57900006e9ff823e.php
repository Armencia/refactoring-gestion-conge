<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/apropos.html.twig */
class __TwigTemplate_25a5ff55d307d8940dda8da19a2bdccff27daa9a1499b8f6383f6d700136abef extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheet' => [$this, 'block_stylesheet'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/apropos.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/apropos.html.twig"));

        // line 2
        $context["NbRestant"] = 0;
        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "home/apropos.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_stylesheet($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheet"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheet"));

        // line 5
        echo "
    <!-- css files -->
    <link href=\"css/bootstrap.css\" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href=\"css/style.css\" rel='stylesheet' type='text/css' /><!-- custom css -->
    <link href=\"css/font-awesome.min.css\" rel=\"stylesheet\"><!-- fontawesome css -->
    <!-- //css files -->

    <!-- google fonts -->
    <link href=\"//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext\" rel=\"stylesheet\">

    <!-- //google fonts -->
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 18
        echo "
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col col-4 offset-2\">
                ";
        // line 22
        if (twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 22, $this->source); })()), "filename", [], "any", false, false, false, 22)) {
            // line 23
            echo "                    ";
            // line 24
            echo "                    <br><br><br><br><img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset((isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 24, $this->source); })()), "imageFile"), "html", null, true);
            echo "\" alt=\"\" style=\"width: 300px; height: auto\">
                ";
        } else {
            // line 26
            echo "                    <img src=\"/images/properties/user.png\" alt=\"\" style=\"width: 300px; height: auto\">
                ";
        }
        // line 28
        echo "                <br>
            </div>
            <div class=\"col col-5\"><br><br><br><br>
                <h5><div> <b>N° Matricule</b> :";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 31, $this->source); })()), "matricule", [], "any", false, false, false, 31), "html", null, true);
        echo "</div>
                    <br>
                    <div> <b>Username</b> :";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 33, $this->source); })()), "username", [], "any", false, false, false, 33), "html", null, true);
        echo "</div>
                    <br>
                    <div> <b>Nom</b> :";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 35, $this->source); })()), "nom", [], "any", false, false, false, 35), "html", null, true);
        echo "</div>
                    <br> <div> <b>Prénom</b> :";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 36, $this->source); })()), "prenom", [], "any", false, false, false, 36), "html", null, true);
        echo "</div>
                    <br><div> <b>Email</b> :";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 37, $this->source); })()), "email", [], "any", false, false, false, 37), "html", null, true);
        echo "</div>
                    <br><div> <b>CIN N°</b> :";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 38, $this->source); })()), "CIN", [], "any", false, false, false, 38), "html", null, true);
        echo "</div>
                    <br><div> <b>Medaille</b> :";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 39, $this->source); })()), "medaille", [], "any", false, false, false, 39), "nom", [], "any", false, false, false, 39), "html", null, true);
        echo "</div>
                    <br><div> <b>Département</b> :";
        // line 40
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 40, $this->source); })()), "departement", [], "any", false, false, false, 40), "nom", [], "any", false, false, false, 40), "html", null, true);
        echo "</div>
                    <br><div> <b>Nombre de Jour Restant</b> :";
        // line 41
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 41, $this->source); })()), "nbJour", [], "any", false, false, false, 41), "html", null, true);
        echo "</div>
                    <br><br></h5>
            </div>
            <br>
        </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/apropos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 41,  154 => 40,  150 => 39,  146 => 38,  142 => 37,  138 => 36,  134 => 35,  129 => 33,  124 => 31,  119 => 28,  115 => 26,  109 => 24,  107 => 23,  105 => 22,  99 => 18,  90 => 17,  69 => 5,  60 => 4,  49 => 1,  47 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% set NbRestant = 0 %}

{% block stylesheet %}

    <!-- css files -->
    <link href=\"css/bootstrap.css\" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href=\"css/style.css\" rel='stylesheet' type='text/css' /><!-- custom css -->
    <link href=\"css/font-awesome.min.css\" rel=\"stylesheet\"><!-- fontawesome css -->
    <!-- //css files -->

    <!-- google fonts -->
    <link href=\"//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext\" rel=\"stylesheet\">

    <!-- //google fonts -->
    {% endblock %}
{% block body %}

    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col col-4 offset-2\">
                {% if user.filename %}
                    {#<img src=\"/images/properties/{{ materiel.filename }}\" alt=\"\" style=\"width: 400px; height: auto\">#}
                    <br><br><br><br><img src=\"{{ vich_uploader_asset(user,'imageFile') }}\" alt=\"\" style=\"width: 300px; height: auto\">
                {% else %}
                    <img src=\"/images/properties/user.png\" alt=\"\" style=\"width: 300px; height: auto\">
                {% endif %}
                <br>
            </div>
            <div class=\"col col-5\"><br><br><br><br>
                <h5><div> <b>N° Matricule</b> :{{ user.matricule }}</div>
                    <br>
                    <div> <b>Username</b> :{{ user.username }}</div>
                    <br>
                    <div> <b>Nom</b> :{{ user.nom }}</div>
                    <br> <div> <b>Prénom</b> :{{ user.prenom }}</div>
                    <br><div> <b>Email</b> :{{ user.email }}</div>
                    <br><div> <b>CIN N°</b> :{{ user.CIN }}</div>
                    <br><div> <b>Medaille</b> :{{ user.medaille.nom }}</div>
                    <br><div> <b>Département</b> :{{ user.departement.nom }}</div>
                    <br><div> <b>Nombre de Jour Restant</b> :{{ user.nbJour }}</div>
                    <br><br></h5>
            </div>
            <br>
        </div>


{% endblock %}", "home/apropos.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\home\\apropos.html.twig");
    }
}
