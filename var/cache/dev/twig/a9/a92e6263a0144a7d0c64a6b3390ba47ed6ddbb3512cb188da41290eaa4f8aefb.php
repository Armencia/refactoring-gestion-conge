<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* conge/showCongeEnvoyer.html.twig */
class __TwigTemplate_d12f3a156213c57287264dec399a1897a7e4bff58e4572b2591c4b9d18d0ed30 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "conge/showCongeEnvoyer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "conge/showCongeEnvoyer.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "conge/showCongeEnvoyer.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Détails demande";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1><center>Détails d'une demande</center></h1>

    <table class=\"table\">
        <tbody>
        <tr>
            <th>Date de demande</th>
            <td>";
        // line 12
        ((twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 12, $this->source); })()), "dateDemande", [], "any", false, false, false, 12)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 12, $this->source); })()), "dateDemande", [], "any", false, false, false, 12), "Y-m-d"), "html", null, true))) : (print ("")));
        echo "</td>
        </tr>
        <tr>
            <th>Matricule</th>
            <td>";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 16, $this->source); })()), "utilisateur", [], "any", false, false, false, 16), "matricule", [], "any", false, false, false, 16), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Nom</th>
            <td>";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 20, $this->source); })()), "utilisateur", [], "any", false, false, false, 20), "nom", [], "any", false, false, false, 20), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Prénoms</th>
            <td>";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 24, $this->source); })()), "utilisateur", [], "any", false, false, false, 24), "prenom", [], "any", false, false, false, 24), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>CIN</th>
            <td>";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 28, $this->source); })()), "CIN", [], "any", false, false, false, 28), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 32, $this->source); })()), "email", [], "any", false, false, false, 32), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Type</th>
            <td>";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 36, $this->source); })()), "typeConge", [], "any", false, false, false, 36), "nom", [], "any", false, false, false, 36), "html", null, true);
        echo "</td>
        </tr>
        ";
        // line 38
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 38, $this->source); })()), "typeConge", [], "any", false, false, false, 38), "nom", [], "any", false, false, false, 38) == "Autre")) {
            // line 39
            echo "            <tr>
                <th>Raison</th>
                <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 41, $this->source); })()), "raison", [], "any", false, false, false, 41), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <th>nombre de Jour</th>
                <td>";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 45, $this->source); })()), "nombre", [], "any", false, false, false, 45), "html", null, true);
            echo "</td>
            </tr>
        ";
        } else {
            // line 48
            echo "            <tr>
                <th>Nombre de Jour</th>
                <td>";
            // line 50
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 50, $this->source); })()), "typeConge", [], "any", false, false, false, 50), "NbJour", [], "any", false, false, false, 50), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        // line 53
        echo "
        <tr>
            <th>Département</th>
            <td>";
        // line 56
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 56, $this->source); })()), "utilisateur", [], "any", false, false, false, 56), "departement", [], "any", false, false, false, 56), "nom", [], "any", false, false, false, 56), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Date de début</th>
            <td>";
        // line 60
        ((twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 60, $this->source); })()), "dateDebut", [], "any", false, false, false, 60)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["conge"]) || array_key_exists("conge", $context) ? $context["conge"] : (function () { throw new RuntimeError('Variable "conge" does not exist.', 60, $this->source); })()), "dateDebut", [], "any", false, false, false, 60)), "html", null, true))) : (print ("")));
        echo "</td>
        </tr>

        </tbody>
    </table>

    ";
        // line 67
        echo "        ";
        // line 68
        echo "        ";
        // line 69
        echo "    ";
        // line 70
        echo "
    <a href=\"";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("conge_index");
        echo "\">
        <button type=\"button\" class=\"btn btn-primary\"> Retour</button>
    </a>
    </br>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "conge/showCongeEnvoyer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 71,  195 => 70,  193 => 69,  191 => 68,  189 => 67,  180 => 60,  173 => 56,  168 => 53,  162 => 50,  158 => 48,  152 => 45,  145 => 41,  141 => 39,  139 => 38,  134 => 36,  127 => 32,  120 => 28,  113 => 24,  106 => 20,  99 => 16,  92 => 12,  84 => 6,  75 => 5,  57 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Détails demande{% endblock %}

{% block body %}
    <h1><center>Détails d'une demande</center></h1>

    <table class=\"table\">
        <tbody>
        <tr>
            <th>Date de demande</th>
            <td>{{ conge.dateDemande ? conge.dateDemande|date('Y-m-d') }}</td>
        </tr>
        <tr>
            <th>Matricule</th>
            <td>{{ conge.utilisateur.matricule }}</td>
        </tr>
        <tr>
            <th>Nom</th>
            <td>{{ conge.utilisateur.nom }}</td>
        </tr>
        <tr>
            <th>Prénoms</th>
            <td>{{ conge.utilisateur.prenom }}</td>
        </tr>
        <tr>
            <th>CIN</th>
            <td>{{ conge.CIN }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ conge.email}}</td>
        </tr>
        <tr>
            <th>Type</th>
            <td>{{ conge.typeConge.nom }}</td>
        </tr>
        {% if conge.typeConge.nom == 'Autre' %}
            <tr>
                <th>Raison</th>
                <td>{{ conge.raison }}</td>
            </tr>
            <tr>
                <th>nombre de Jour</th>
                <td>{{ conge.nombre }}</td>
            </tr>
        {% else %}
            <tr>
                <th>Nombre de Jour</th>
                <td>{{ conge.typeConge.NbJour }}</td>
            </tr>
        {% endif %}

        <tr>
            <th>Département</th>
            <td>{{  conge.utilisateur.departement.nom }}</td>
        </tr>
        <tr>
            <th>Date de début</th>
            <td>{{ conge.dateDebut ? conge.dateDebut|date() }}</td>
        </tr>

        </tbody>
    </table>

    {#<div>#}
        {#<a href=\"{{ path('mailer') }}\" class=\"btn btn-success\">Accepter</a>#}
        {#<a href=\"{{ path('mailerRefuser') }}\" class=\"btn btn-danger\">Refuser</a>#}
    {#</div><br>#}

    <a href=\"{{ path('conge_index') }}\">
        <button type=\"button\" class=\"btn btn-primary\"> Retour</button>
    </a>
    </br>



{% endblock %}
", "conge/showCongeEnvoyer.html.twig", "C:\\wamp64\\www\\Gestion_Conge-master\\templates\\conge\\showCongeEnvoyer.html.twig");
    }
}
