<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/booking' => [[['_route' => 'booking_index', '_controller' => 'App\\Controller\\BookingController::index'], null, ['GET' => 0], null, true, false, null]],
            '/booking/new' => [[['_route' => 'booking_new', '_controller' => 'App\\Controller\\BookingController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/conge' => [[['_route' => 'conge_index', '_controller' => 'App\\Controller\\CongeController::index'], null, ['GET' => 0], null, true, false, null]],
            '/conge/new-test' => [[['_route' => 'conge_new', '_controller' => 'App\\Controller\\CongeController::newConge'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/conge/conger/envoyer' => [[['_route' => 'indexCogeEnvoyer', '_controller' => 'App\\Controller\\CongeController::indexCongeEnvoyer'], null, null, null, false, false, null]],
            '/conge/conger/recue' => [[['_route' => 'indexCongeRecu', '_controller' => 'App\\Controller\\CongeController::indexCongeRecu'], null, null, null, false, false, null]],
            '/conge/ swift/conge/accepter' => [[['_route' => 'mailer', '_controller' => 'App\\Controller\\CongeController::indexSwift'], null, null, null, false, false, null]],
            '/conge/swift/conge/refuser' => [[['_route' => 'mailerRefuser', '_controller' => 'App\\Controller\\CongeController::indexSwiftRefuser'], null, null, null, false, false, null]],
            '/departement' => [[['_route' => 'departement_index', '_controller' => 'App\\Controller\\DepartementController::index'], null, ['GET' => 0], null, true, false, null]],
            '/departement/new' => [[['_route' => 'departement_new', '_controller' => 'App\\Controller\\DepartementController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/dompdf' => [[['_route' => 'dompdf', '_controller' => 'App\\Controller\\DompdfController::toPdfAction'], null, null, null, false, false, null]],
            '/gerer' => [[['_route' => 'gerer', '_controller' => 'App\\Controller\\GererController::index'], null, null, null, false, false, null]],
            '/gestion' => [[['_route' => 'gestion', '_controller' => 'App\\Controller\\GestionController::index'], null, null, null, false, false, null]],
            '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
            '/interface/calendrier' => [[['_route' => 'interface_calendrier', '_controller' => 'App\\Controller\\InterfaceCalendrierController::index'], null, null, null, false, false, null]],
            '/medaille' => [[['_route' => 'medaille_index', '_controller' => 'App\\Controller\\MedailleController::index'], null, ['GET' => 0], null, true, false, null]],
            '/medaille/new' => [[['_route' => 'medaille_new', '_controller' => 'App\\Controller\\MedailleController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/permission' => [[['_route' => 'permission_index', '_controller' => 'App\\Controller\\PermissionController::index'], null, ['GET' => 0], null, true, false, null]],
            '/permission/new' => [[['_route' => 'permission_new', '_controller' => 'App\\Controller\\PermissionController::newPermission'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/permission/permission/envoyer' => [[['_route' => 'indexPermissionEnvoyer', '_controller' => 'App\\Controller\\PermissionController::indexCongeEnvoyer'], null, null, null, false, false, null]],
            '/permission/permission/recue' => [[['_route' => 'indexPermissionRecu', '_controller' => 'App\\Controller\\PermissionController::indexCongeRecu'], null, null, null, false, false, null]],
            '/permission/ swift/permission/accepter' => [[['_route' => 'mailerPermission', '_controller' => 'App\\Controller\\PermissionController::indexSwift'], null, null, null, false, false, null]],
            '/permission/swift/refuser' => [[['_route' => 'mailerRefuserPermission', '_controller' => 'App\\Controller\\PermissionController::indexSwiftRefuser'], null, null, null, false, false, null]],
            '/planning' => [[['_route' => 'planning', '_controller' => 'App\\Controller\\PlanningController::index'], null, null, null, false, false, null]],
            '/recherche' => [[['_route' => 'recherche', '_controller' => 'App\\Controller\\RechercheController::index'], null, null, null, false, false, null]],
            '/login' => [[['_route' => 'login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
            '/logouta' => [[['_route' => 'logouta', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
            '/verifyLogin' => [[['_route' => 'verify_login', '_controller' => 'App\\Controller\\SecurityController::verifyLogin'], null, null, null, false, false, null]],
            '/profil' => [[['_route' => 'profil', '_controller' => 'App\\Controller\\SecurityController::profil'], null, null, null, false, false, null]],
            '/apropos' => [[['_route' => 'apropos', '_controller' => 'App\\Controller\\SecurityController::propos'], null, null, null, false, false, null]],
            '/test' => [[['_route' => 'test', '_controller' => 'App\\Controller\\SecurityController::test'], null, null, null, false, false, null]],
            '/bookings/calendar' => [[['_route' => 'booking', '_controller' => 'App\\Controller\\SecurityController::booking'], null, null, null, false, false, null]],
            '/accueil' => [[['_route' => 'accueil', '_controller' => 'App\\Controller\\SecurityController::accueil'], null, null, null, false, false, null]],
            '/snappy' => [[['_route' => 'snappy', '_controller' => 'App\\Controller\\SnappyController::indexAction'], null, null, null, false, false, null]],
            '/type/conge' => [[['_route' => 'type_conge_index', '_controller' => 'App\\Controller\\TypeCongeController::index'], null, ['GET' => 0], null, true, false, null]],
            '/type/conge/new' => [[['_route' => 'type_conge_new', '_controller' => 'App\\Controller\\TypeCongeController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/type/permission' => [[['_route' => 'type_permission_index', '_controller' => 'App\\Controller\\TypePermissionController::index'], null, ['GET' => 0], null, true, false, null]],
            '/type/permission/new' => [[['_route' => 'type_permission_new', '_controller' => 'App\\Controller\\TypePermissionController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/utilisateur' => [[['_route' => 'utilisateur_index', '_controller' => 'App\\Controller\\UtilisateurController::index'], null, ['GET' => 0], null, true, false, null]],
            '/utilisateur/accueil' => [[['_route' => 'utilisateur_accueil', '_controller' => 'App\\Controller\\UtilisateurController::accueil'], null, ['GET' => 0], null, false, false, null]],
            '/utilisateur/new' => [[['_route' => 'utilisateur_new', '_controller' => 'App\\Controller\\UtilisateurController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/fc-load-events' => [[['_route' => 'fullcalendar_load_events', '_controller' => 'Toiba\\FullCalendarBundle\\Controller\\CalendarController::loadAction'], null, null, null, false, false, null]],
            '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
            '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
            '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
            '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
            '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
            '/logout' => [[['_route' => 'logout'], null, null, null, false, false, null]],
        ];
        $this->regexpList = [
            0 => '{^(?'
                    .'|/booking/([^/]++)(?'
                        .'|(*:27)'
                        .'|/edit(*:39)'
                        .'|(*:46)'
                    .')'
                    .'|/conge/(?'
                        .'|new/dompdf/([^/]++)(*:83)'
                        .'|pdf/([^/]++)(*:102)'
                        .'|([^/]++)(*:118)'
                        .'|envoyer/([^/]++)(*:142)'
                        .'|recue/([^/]++)(*:164)'
                        .'|([^/]++)(?'
                            .'|/edit(*:188)'
                            .'|(*:196)'
                        .')'
                        .'|envoyera/([^/]++)(*:222)'
                    .')'
                    .'|/departement/([^/]++)(?'
                        .'|(*:255)'
                        .'|/edit(*:268)'
                        .'|(*:276)'
                    .')'
                    .'|/medaille/([^/]++)(?'
                        .'|(*:306)'
                        .'|/edit(*:319)'
                        .'|(*:327)'
                    .')'
                    .'|/permission/(?'
                        .'|envoyer/parmission/([^/]++)(*:378)'
                        .'|([^/]++)(*:394)'
                        .'|pemission/(?'
                            .'|envoyer/([^/]++)(*:431)'
                            .'|recue/([^/]++)(*:453)'
                        .')'
                        .'|([^/]++)(?'
                            .'|/edit(*:478)'
                            .'|(*:486)'
                        .')'
                        .'|new/Permission/dompdf/([^/]++)(*:525)'
                    .')'
                    .'|/type/(?'
                        .'|conge/([^/]++)(?'
                            .'|(*:560)'
                            .'|/edit(*:573)'
                            .'|(*:581)'
                        .')'
                        .'|permission/([^/]++)(?'
                            .'|(*:612)'
                            .'|/edit(*:625)'
                            .'|(*:633)'
                        .')'
                    .')'
                    .'|/utilisateur/([^/]++)(?'
                        .'|(*:667)'
                        .'|/edit(*:680)'
                        .'|(*:688)'
                    .')'
                    .'|/js/routing(?:\\.(js|json))?(*:724)'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:763)'
                        .'|wdt/([^/]++)(*:783)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:829)'
                                .'|router(*:843)'
                                .'|exception(?'
                                    .'|(*:863)'
                                    .'|\\.css(*:876)'
                                .')'
                            .')'
                            .'|(*:886)'
                        .')'
                    .')'
                .')/?$}sDu',
        ];
        $this->dynamicRoutes = [
            27 => [[['_route' => 'booking_show', '_controller' => 'App\\Controller\\BookingController::show'], ['id'], ['GET' => 0], null, false, true, null]],
            39 => [[['_route' => 'booking_edit', '_controller' => 'App\\Controller\\BookingController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
            46 => [[['_route' => 'booking_delete', '_controller' => 'App\\Controller\\BookingController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            83 => [[['_route' => 'conge_pdf', '_controller' => 'App\\Controller\\CongeController::toPdfAction'], ['id'], null, null, false, true, null]],
            102 => [[['_route' => 'pdf_test', '_controller' => 'App\\Controller\\CongeController::test'], ['id'], ['GET' => 0], null, false, true, null]],
            118 => [[['_route' => 'conge_show', '_controller' => 'App\\Controller\\CongeController::show'], ['id'], ['GET' => 0], null, false, true, null]],
            142 => [[['_route' => 'congeEnvoyer_show', '_controller' => 'App\\Controller\\CongeController::showEnvoyer'], ['id'], ['GET' => 0], null, false, true, null]],
            164 => [[['_route' => 'congeRecue_show', '_controller' => 'App\\Controller\\CongeController::showRecue'], ['id'], ['GET' => 0], null, false, true, null]],
            188 => [[['_route' => 'conge_edit', '_controller' => 'App\\Controller\\CongeController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
            196 => [[['_route' => 'conge_delete', '_controller' => 'App\\Controller\\CongeController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            222 => [[['_route' => 'envoyerCongeaa', '_controller' => 'App\\Controller\\CongeController::envoyer'], ['id'], null, null, false, true, null]],
            255 => [[['_route' => 'departement_show', '_controller' => 'App\\Controller\\DepartementController::show'], ['id'], ['GET' => 0], null, false, true, null]],
            268 => [[['_route' => 'departement_edit', '_controller' => 'App\\Controller\\DepartementController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
            276 => [[['_route' => 'departement_delete', '_controller' => 'App\\Controller\\DepartementController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            306 => [[['_route' => 'medaille_show', '_controller' => 'App\\Controller\\MedailleController::show'], ['id'], ['GET' => 0], null, false, true, null]],
            319 => [[['_route' => 'medaille_edit', '_controller' => 'App\\Controller\\MedailleController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
            327 => [[['_route' => 'medaille_delete', '_controller' => 'App\\Controller\\MedailleController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            378 => [[['_route' => 'envoyerPermission', '_controller' => 'App\\Controller\\PermissionController::envoyer'], ['id'], null, null, false, true, null]],
            394 => [[['_route' => 'permission_show', '_controller' => 'App\\Controller\\PermissionController::show'], ['id'], ['GET' => 0], null, false, true, null]],
            431 => [[['_route' => 'permissionEnvoyer_show', '_controller' => 'App\\Controller\\PermissionController::showEnvoyer'], ['id'], ['GET' => 0], null, false, true, null]],
            453 => [[['_route' => 'permissionRecue_show', '_controller' => 'App\\Controller\\PermissionController::showRecue'], ['id'], ['GET' => 0], null, false, true, null]],
            478 => [[['_route' => 'permission_edit', '_controller' => 'App\\Controller\\PermissionController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
            486 => [[['_route' => 'permission_delete', '_controller' => 'App\\Controller\\PermissionController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            525 => [[['_route' => 'Permission_pdf', '_controller' => 'App\\Controller\\PermissionController::toPdfAction'], ['id'], null, null, false, true, null]],
            560 => [[['_route' => 'type_conge_show', '_controller' => 'App\\Controller\\TypeCongeController::show'], ['id'], ['GET' => 0], null, false, true, null]],
            573 => [[['_route' => 'type_conge_edit', '_controller' => 'App\\Controller\\TypeCongeController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
            581 => [[['_route' => 'type_conge_delete', '_controller' => 'App\\Controller\\TypeCongeController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            612 => [[['_route' => 'type_permission_show', '_controller' => 'App\\Controller\\TypePermissionController::show'], ['id'], ['GET' => 0], null, false, true, null]],
            625 => [[['_route' => 'type_permission_edit', '_controller' => 'App\\Controller\\TypePermissionController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
            633 => [[['_route' => 'type_permission_delete', '_controller' => 'App\\Controller\\TypePermissionController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            667 => [[['_route' => 'utilisateur_show', '_controller' => 'App\\Controller\\UtilisateurController::show'], ['id'], ['GET' => 0], null, false, true, null]],
            680 => [[['_route' => 'utilisateur_edit', '_controller' => 'App\\Controller\\UtilisateurController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
            688 => [[['_route' => 'utilisateur_delete', '_controller' => 'App\\Controller\\UtilisateurController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
            724 => [[['_route' => 'fos_js_routing_js', '_controller' => 'fos_js_routing.controller:indexAction', '_format' => 'js'], ['_format'], ['GET' => 0], null, false, true, null]],
            763 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
            783 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
            829 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
            843 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
            863 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
            876 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
            886 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        ];
    }
}
