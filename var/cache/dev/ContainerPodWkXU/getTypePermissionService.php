<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.errored..service_locator.pgEBe2k.App\Entity\TypePermission' shared service.

include_once $this->targetDirs[3].'\\src\\Entity\\TypePermission.php';

return $this->privates['.errored..service_locator.pgEBe2k.App\\Entity\\TypePermission'] = new \App\Entity\TypePermission();
